//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef CELL_H
#define CELL_H

#include <SFML/Graphics.hpp>

namespace flc_GameBoard
{

class Cell
{
public:
  // Attributes ----------------------------------------------------------------
  enum ContentType
  {
    None,
    Hit,
    Miss,
    DestroyedBoat
  };
  // Methods -------------------------------------------------------------------
  Cell ();
  explicit Cell (sf::IntRect bounds);
  virtual ~Cell();
  sf::IntRect getBounds() const { return m_bounds; }
  void setBounds (sf::IntRect bounds) { m_bounds = bounds; }
  unsigned int getRow() const { return m_row; }
  void setRow (unsigned int row) { m_row = row; }
  unsigned int getCol() const { return m_col; }
  void setCol (unsigned int col) { m_col = col; }
  void setRowCol (unsigned int row, unsigned int col) { setRow (row); setCol (col); }
  sf::Vector2i getRowColl() const;
  sf::Vector2f getPosition() const;
  ContentType getContent() const { return m_contentType; }
  void setContent (ContentType v) { m_contentType = v; }

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  unsigned int m_row;
  unsigned int m_col;
  sf::IntRect m_bounds;
  ContentType m_contentType;
};

}

#endif // CELL_H
