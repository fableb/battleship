//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "component.h"

namespace flc_GUI
{

Component::Component():
  m_isVisible{true},
  m_isSelected{false},
  m_isActive{false}
{
  //ctor
}

Component::~Component()
{
  //dtor
}

bool Component::isSelected() const
{
  return m_isSelected;
}

void Component::select()
{
  m_isSelected = true;
}

void Component::deselect()
{
  m_isSelected = false;
}

bool Component::isActive() const
{
  return m_isActive;
}

void Component::activate()
{
  m_isActive = true;
}

void Component::deactivate()
{
  m_isActive = false;
}

bool Component::isVisible() const
{
  return m_isVisible;
}

void Component::setIsVisible (bool flag)
{
  m_isVisible = flag;
}

}

