//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "actor.h"

Actor::Actor():
  m_id{0},
  m_isSelected{false}
{

}

Actor::~Actor()
{

}

std::string Actor::getPositionAsString (sf::Vector2i position)
{
  std::string res = "x:" + std::to_string (position.x) +
                    "|y:" + std::to_string (position.y);
  return res;
}

std::string Actor::getBoundsAsString (sf::FloatRect bounds)
{
  std::string res = "x:" + std::to_string (bounds.left) +
                    "|y:" + std::to_string (bounds.top) +
                    "|width:" + std::to_string (bounds.width) +
                    "|height:" + std::to_string (bounds.height);
  return res;
}
