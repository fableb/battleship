//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <cstdlib>
#include <ctime>
#include <memory>
#include <iostream>
#include <vector>
#include <deque>
#include <random>
#include <algorithm>
#include <SFML/Graphics.hpp>
#include "state.h"
#include "globals.h"
#include "resourceholder.h"
#include "musicplayer.h"
#include "board.h"
#include "boat.h"
#include "missile.h"
#include "fleetgenarator.h"
#include "hunttargets.h"
#include "container.h"
#include "button.h"

namespace flc_game
{

class GameState: public State
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  GameState (StateManager *stateManager, ApplicationContext context);
  virtual ~GameState();

  void init() {};
  void destroy() {};
  void processEvents (const sf::Event &e); // game control layer
  void update (sf::Time &dt); // game logic layer
  void render(); // game view layer

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  enum class HuntMode
  {
    Normal,
    Intensive
  };
  enum class GameStates
  {
    BoatsPositioning,
    Running,
    GameOver
  };
  GameStates m_gameState;
  enum class PlayerTurn
  {
    None,
    Player1,
    Player2
  };
  PlayerTurn m_playerTurn;
  bool m_isPaused; // game window has the focus or not

  // UI
  sf::Sprite m_BoardBackgroundSprite;

  sf::Text m_GameOverText;
  std::wstring m_gameOverTextWinStr = L"Vous avez gagné !";
  std::wstring m_gameOverTextLooseStr = L"Vous avez perdu !";

  sf::Sprite m_introSprite;
  sf::Text m_introText;
  std::wstring m_introTextStr =
    LR"(Placer vos bateaux en utilisant le
BOUTON GAUCHE de la souris.

Vous pouvez demander le placement
automatique de vos bateaux en
cliquant sur 'AUTO.'. Vous pouvez
ensuite changer l'emplacement de chaque
navire.

Utiliser le BOUTON DROIT de la souris
pour effectuer une rotation du bateau.

Si votre bateau n'effectue pas la rotation
c'est qu'il manque de place. Déplacez le
et recommencez.

Une fois le jeu lancé, utiliser le BOUTON
GAUCHE de votre souris pour envoyer une
bombe contre votre adversaire.

Pour continuer, sélectionner le joueur
qui débute la partie ('VOUS' ou 'ADVERSAIRE')...
)";

  sf::Text m_startText;
  std::wstring m_startTextStr =
LR"(Cliquez sur cette grille
pour débuter la partie
)";
  bool m_isStartTextDisplayed;

  flc_GUI::Container m_GUIContainerPositioning;
  flc_GUI::Container m_GUIContainerGameRunning;
  flc_GUI::Container m_GUIContainerGameOver;

  sf::Text m_P1MissilesText;
  std::wstring m_missilesTextMessage = L"Missiles: ";

  unsigned int m_P1GlobalHealth;
  unsigned int m_P2GlobalHealth;
  flc_GameBoard::Board m_P1Board; // player 1 board
  flc_GameBoard::Board m_P2Board; // player 2 board
  std::vector<std::unique_ptr<Boat>> m_P1Boats; // player 1 fleet
  std::vector<std::unique_ptr<Boat>> m_P2Boats; // player 2 fleet
  std::vector<std::unique_ptr<Missile>> m_P1Missiles; // player 1 missiles
  std::vector<std::unique_ptr<Missile>> m_P2Missiles; // player 2 missiles
  std::deque<unsigned int> m_P2MissileOrderList; // missiles' id fire order that will be shuffled or set by AI
  HuntTargets m_P2HuntTargets; // AI targets during a hunt (when a boat is damaged)

  bool m_isBoatMove;
  bool m_isBoatRotation;
  bool m_isMissileMove;
  int m_P1BoatId; // selected Boat by mouse left click
  ActorId m_P1MissileId; // selected Missile by mouse left click
  ActorId m_P2MissileId;
  int m_dx;
  int m_dy;
  sf::Vector2i m_currentMousePosition; // current mouse position in the game
  sf::Vector2f m_P1BoatOldPosition; // selected boat old position for mouse drag and drop
  sf::Vector2f m_P1BoatNewPosition; // selected boat new position for mouse drag and drop
  sf::Vector2f m_P1MissileNewPosition;
  sf::Vector2f m_P2MissileNewPosition;
  HuntMode m_P2HuntMode;
  float m_duration; // for game over blinking text
  flc_FeetGenerator::FleetGenarator m_fleetGenerator; // Create all possible positions for each boat
  // Methods -------------------------------------------------------------------
  void handlePlayerInput (sf::Keyboard::Key key, bool isPressed);
  void handlePlayerMouse (sf::Event::MouseButtonEvent mouseButtonEvent, sf::Vector2i &mousePosition, bool isButtonPressed);
  bool checkBoatRotationCollision (ActorId boatId, std::vector<std::unique_ptr<Boat>> &boats,  flc_GameBoard::Board &Board);
  bool checkMarginCollision (const std::unique_ptr<Boat> &actor, flc_GameBoard::Board &board);
  sf::Vector2<int> getClickedCell();
  void initMissileOrderList();
  void removeFromMissileOrderList (unsigned int missileId);
  int searchHit(); // search for damaged boats, but not destroyed yet
  void initHuntTargets (unsigned int missileId);
//  bool isUselessTarget (unsigned int missileId);
//  int computeSpaceIndex(unsigned int missileId, int delta); // if < 0 the space with radius delta around missileId is filled
  void updateMissilesAnimation (sf::Time dt);
  void boatsAutoPlacement (std::vector<std::unique_ptr<Boat>> &boats, flc_GameBoard::Board &board);
  bool m_updateFirstPass = false;
};

#include "gamestate.inl"

}

#endif // GAMESTATE_H
