//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "battlefield.h"

namespace flc_GameBoard
{

Battlefield::Battlefield():
  m_numRows{0},
  m_numCols{0},
  m_tileSize{0},
  m_cells{nullptr}
{

}

Battlefield::~Battlefield()
{
  destroyBounds();
}

void Battlefield::setBounds (sf::IntRect bounds, unsigned int numRows, unsigned int numCols, unsigned int tileSize)
{
  // Reset battlefield and battlefield cells
  destroyBounds();
  // Set battlefield boundsn and other stuff
  m_bounds = bounds;
  m_numRows = numRows;
  m_numCols = numCols;
  m_tileSize = tileSize;
  // Create and set battlefield cells
  initBounds();
}

void Battlefield::initBounds()
{
  // Create cells, set their bounds
  m_cells = new Cell*[m_numRows]; // numRows of pointers on Cell
  for (unsigned int i = 0; i < m_numRows; i++)
    m_cells[i] = new Cell[m_numCols]; // array of numCols of Cell (numRows times)

  for (unsigned int i = 0; i < m_numRows; i++)
  {
    for (unsigned int j = 0; j < m_numCols; j++)
    {
      m_cells[i][j].setBounds (sf::IntRect (m_bounds.left + j * m_tileSize, m_bounds.top + i * m_tileSize, m_tileSize, m_tileSize));
      m_cells[i][j].setRowCol (i, j);
    }
  }
}

void Battlefield::destroyBounds()
{
  if (m_cells)
  {
    for (unsigned int i = 0; i < m_numRows; i++)
    {
      if (m_cells[i])
      {
        delete [] m_cells[i];
        m_cells[i] = nullptr;
      }
    }
    delete [] m_cells;
    m_cells = nullptr;
  }

  m_bounds.left = 0;
  m_bounds.top = 0;
  m_bounds.width = 0;
  m_bounds.height = 0;
  m_numRows = 0;
  m_numCols = 0;
  m_tileSize = 0;
}

}
