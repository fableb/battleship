//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "globals.h"
#include "resourceholder.h"
#include "soundplayer.h"
#include "statemanager.h"

int main (int argc, char **argv)
{
  sf::RenderWindow window (sf::VideoMode (APP_WIDTH, APP_HEIGHT), "FLC-Battleship (v1.0)", sf::Style::Titlebar | sf::Style::Close);

  // Load textures
  TextureHolder textures;
  textures.load (Textures::Intro, "assets/textures/intro.png");
  textures.load (Textures::BoardBackground, "assets/textures/board_bg_64x64.png");
  textures.load (Textures::BoardBlue, "assets/textures/board_blue_64x64.png");
  textures.load (Textures::BoardOrange, "assets/textures/board_orange_64x64.png");
  textures.load (Textures::ForegroundBlue, "assets/textures/fg_blue_64x64.png");
  textures.load (Textures::ButtonOrange0, "assets/textures/red_button10.png");
  textures.load (Textures::ButtonOrange1, "assets/textures/red_button11.png");
  textures.load (Textures::ButtonOrange2, "assets/textures/red_button12.png");
  textures.load (Textures::Destroyer, "assets/textures/destroyer2_64x64.png");
  textures.load (Textures::Submarine, "assets/textures/submarine2_64x64.png");
  textures.load (Textures::Cruiser, "assets/textures/cruiser2_64x64.png");
  textures.load (Textures::Battleship, "assets/textures/battleship2_64x64.png");
  textures.load (Textures::Carrier, "assets/textures/carrier2_64x64.png");
  textures.load (Textures::Missile, "assets/textures/missile3_64x64_no_border.png");
  textures.load (Textures::MissileExplosion, "assets/textures/missile_explosion_64x64.png");
  textures.load (Textures::MissileFire, "assets/textures/missile_fire1_64x64.png");

  // Load fonts
  FontHolder fonts;
  fonts.load (Fonts::Normal, "assets/fonts/kenney_rocket_square.ttf");
  fonts.load (Fonts::GameOver, "assets/fonts/black_metal_sans.ttf");

  // Loas musics
  MusicPlayer musics;

  // Load sounds
  SoundPlayer sounds;

  // Set application context to be passed to all states
  ApplicationContext context (window, textures, fonts, musics, sounds);
  context.m_window->setFramerateLimit (NUM_FRAMES); // for GPU 3D usage
  context.m_window->setVerticalSyncEnabled (true); // for GPU 3D usage

  // Run game through state manager
  flc_game::StateManager sm (context);
  sm.run();
  return 0;
}
