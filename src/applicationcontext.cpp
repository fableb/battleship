//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "applicationcontext.h"

ApplicationContext::ApplicationContext (sf::RenderWindow &window, TextureHolder &textures, FontHolder &fonts, MusicPlayer &musics, SoundPlayer &sounds):
  m_window{&window},
  m_textures{&textures},
  m_fonts{&fonts},
  m_musics{&musics},
  m_sounds{&sounds}
{
  //ctor
}

ApplicationContext::~ApplicationContext()
{
  //dtor
}
