//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
template <typename Resource, typename Identifier>
void ResourceHolder<Resource, Identifier>::insertResource (Identifier id, std::unique_ptr<Resource> resource)
{
  auto inserted = m_resourceMap.insert (std::make_pair (id, std::move (resource)));
  assert (inserted.second);
}

template <typename Resource, typename Identifier>
void ResourceHolder<Resource, Identifier>::load (Identifier id, const std::string &filename)
{
  std::unique_ptr<Resource> resource (new Resource());
  if (!resource->loadFromFile (filename))
    throw std::runtime_error ("ResourceHolder::load - Failed to load " + filename);
  insertResource (id, std::move (resource));
}

template <typename Resource, typename Identifier>
template <typename Parameter>
void ResourceHolder<Resource, Identifier>::load (Identifier id, const std::string& filename, const Parameter& secondParam)
{
  std::unique_ptr<Resource> resource (new Resource());
  if (!resource->loadFromFile (filename, secondParam))
    throw std::runtime_error ("ResourceHolder::load - Failed to load " + filename);

  insertResource (id, std::move (resource));
}

template <typename Resource, typename Identifier>
Resource &ResourceHolder<Resource, Identifier>::get (Identifier id)
{
  auto found = m_resourceMap.find (id);
  assert (found != m_resourceMap.end());
  return *found->second;
}

template <typename Resource, typename Identifier>
const Resource &ResourceHolder<Resource, Identifier>::get (Identifier id) const
{
  auto found = m_resourceMap.find (id);
  assert (found != m_resourceMap.end());
  return *found->second;
}
