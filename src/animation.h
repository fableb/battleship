//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef ANIMATION_H
#define ANIMATION_H

#include <SFML/Graphics.hpp>

//
// Code from Laurent GOMILA, 'SFML Game Development', Packt Publishing
//
class Animation: public sf::Drawable, public sf::Transformable
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  Animation();
  explicit Animation (const sf::Texture &texture);
  virtual ~Animation();
  void setTexture (const sf::Texture &texture) { m_sprite.setTexture (texture); }
  const sf::Texture *getTexture() const { return m_sprite.getTexture(); }
  void setFrameSize (sf::Vector2i frameSize) { m_frameSize = frameSize; }
  sf::Vector2i getFrameSize() const { return m_frameSize; }
  void setNumFrames (std::size_t numFrames) { m_numFrames = numFrames; }
  std::size_t getNumFrames() const { return m_numFrames; }
  void setDuration (sf::Time duration) { m_duration = duration; }
  sf::Time getDuration() const { return m_duration; }
  void setRepeating (bool v) { m_repeat = v; }
  bool isRepeating() const { return m_repeat; }
  void restart() { m_currentFrame = 0; }
  bool isFinished() const;
  sf::FloatRect getLocalBounds() const;
  sf::FloatRect getGlobalBounds() const;
  void update (sf::Time dt);

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  sf::Sprite m_sprite;
  sf::Vector2i m_frameSize;
  std::size_t m_numFrames;
  std::size_t m_currentFrame;
  sf::Time m_duration;
  sf::Time m_elapsedTime;
  bool m_repeat;
  // Methods -------------------------------------------------------------------
  void draw (sf::RenderTarget &target, sf::RenderStates states) const;
};

#endif // ANIMATION_H
