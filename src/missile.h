//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef MISSILE_H
#define MISSILE_H

#include <iostream>
#include <actor.h>
#include "globals.h"
#include "animation.h"

class Missile : public Actor
{
public:
  // Attributes ----------------------------------------------------------------
  enum Status
  {
    None,
    Hit,
    Miss
  };

  // Methods -------------------------------------------------------------------
  explicit Missile (unsigned int id);
  virtual ~Missile();
  Missile::Status getStatus() const { return m_status; }
  void setStatus (Missile::Status status);
  void updateOccupiedCells (flc_GameBoard::Battlefield &b);
  void setTargetRowCol (unsigned int row, unsigned int col) { m_targetRowCol = sf::Vector2i (row, col); }
  sf::Vector2i getTargetRowCol() const { return m_targetRowCol; }
  Animation &getExplosionAnimation() { return m_explosion; }
  Animation &getFireAnimation() { return m_fire; }

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  Missile::Status m_status;
  sf::Vector2i m_targetRowCol;
  Animation m_explosion;
  Animation m_fire;
  // Methods -------------------------------------------------------------------
  std::string occupiedCellsAsString();
};

#endif // MISSILE_H
