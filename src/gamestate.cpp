//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "gamestate.h"
#include "statemanager.h"

namespace flc_game
{

void createBoats (std::vector<std::unique_ptr<Boat>> &boats, unsigned int, TextureHolder &);
void createMissiles (std::vector<std::unique_ptr<Missile>> &missiles, unsigned int numMissiles, TextureHolder &textures, unsigned int tileSize);
int rowColToIndex (int row, int col, int colDim);
int rowColToIndex (sf::Vector2i rowCol, int colDim);
sf::Vector2i indexToRowCol (int index, int colDim);
bool isValidRowCol (sf::Vector2i rowCol, sf::Vector2i rowBounds, sf::Vector2i colBounds);

GameState::GameState (StateManager *stateManager, ApplicationContext context) :
  State (stateManager, context),
  m_gameState{ GameStates::BoatsPositioning },
  m_playerTurn{ PlayerTurn::None },
  m_isPaused{ false },
  m_isStartTextDisplayed{ false },
  m_P1GlobalHealth{ 17 },
  m_P2GlobalHealth{ 17 },
  m_isBoatMove{ false },
  m_isBoatRotation{ false },
  m_isMissileMove{ false },
  m_P1BoatId{ -1 },
  m_P1MissileId{ 0 },
  m_P2MissileId{ 0 },
  m_dx{ 0 },
  m_dy{ 0 },
  m_currentMousePosition(),
  m_P1BoatOldPosition(),
  m_P1BoatNewPosition(),
  m_P1MissileNewPosition(),
  m_P2MissileNewPosition(),
  m_P2HuntMode{HuntMode::Normal},
  m_duration{0.f},
  m_fleetGenerator (NUM_COLS_AND_ROWS, NUM_COLS_AND_ROWS)
{
  // Board background **********************************************************
  m_BoardBackgroundSprite.setTexture (m_context.m_textures->get (Textures::BoardBackground));

  // Introductory text *********************************************************
  m_introText.setFont (m_context.m_fonts->get (Fonts::Normal));
  m_introText.setString (m_introTextStr);
  m_introText.setCharacterSize (INTRO_TEXT_SIZE);
  m_introText.setFillColor (sf::Color::Black);
  m_introText.setPosition (INTRO_TEXT_POSX, INTRO_TEXT_POSY);

  m_introSprite.setTexture (m_context.m_textures->get (Textures::ForegroundBlue));
  m_introSprite.setTextureRect (sf::IntRect (0, 0, FULL_BOARD_SIZE_WIDTH, FULL_BOARD_SIZE_HEIGHT));
  m_introSprite.setPosition (FULL_BOARD_SIZE_WIDTH, 0.f);

  // Click here to start game **************************************************
  m_startText.setFont (m_context.m_fonts->get (Fonts::Normal));
  m_startText.setString (m_startTextStr);
  m_startText.setCharacterSize (INTRO_TEXT_SIZE);
  m_startText.setFillColor (sf::Color::Black);
  m_startText.setPosition (P2_BOARD_POSX + m_startText.getLocalBounds().width / 2, P2_BOARD_POSY + BOARD_SIZE / 2 - m_startText.getLocalBounds().height / 2);

  // Player 1 button ***********************************************************
  auto P1StartsButton = std::make_shared<flc_GUI::Button> (*m_context.m_fonts, *m_context.m_textures);
  P1StartsButton->setPosition (m_introText.getGlobalBounds().left, m_introText.getGlobalBounds().height + P1StartsButton->getGlobalBounds().height + 20);
  P1StartsButton->setText (L"Vous");
  P1StartsButton->setCallback ([this]()
  {
    std::cout << "Player1 starts callback" << std::endl;
    m_currentMousePosition.x = -1;
    m_currentMousePosition.y = -1;
    m_context.m_musics->stop();
    m_playerTurn = PlayerTurn::Player1;
    m_gameState = GameStates::Running;
  });
  m_GUIContainerPositioning.pack (P1StartsButton);

  // Player 2 button ***********************************************************
  auto P2StartsButton = std::make_shared<flc_GUI::Button> (*m_context.m_fonts, *m_context.m_textures);
  P2StartsButton->setPosition (P1StartsButton->getGlobalBounds().left + P1StartsButton->getLocalBounds().width + 5, m_introText.getGlobalBounds().height + P2StartsButton->getGlobalBounds().height + 20);
  P2StartsButton->setText (L"Adversaire");
  P2StartsButton->setCallback ([this]()
  {
    std::cout << "Player2 starts callback" << std::endl;
    m_currentMousePosition.x = -1;
    m_currentMousePosition.y = -1;
    m_context.m_musics->stop();
    m_playerTurn = PlayerTurn::Player2;
    m_gameState = GameStates::Running;
  });
  m_GUIContainerPositioning.pack (P2StartsButton);

  // Boats auto placement button ***********************************************
  auto boatsAutoButton = std::make_shared<flc_GUI::Button> (*m_context.m_fonts, *m_context.m_textures);
  boatsAutoButton->setPosition (FULL_BOARD_SIZE_WIDTH - boatsAutoButton->getLocalBounds().width - 5, APP_HEIGHT - boatsAutoButton->getLocalBounds().height - 5);
  boatsAutoButton->setText (L"Auto.");
  boatsAutoButton->setCallback ([this]()
  {
    std::cout << "Boats auto placement callback" << std::endl;
    boatsAutoPlacement (m_P1Boats, m_P1Board);
  });
  m_GUIContainerPositioning.pack (boatsAutoButton);

  // Intro button **************************************************************
  auto introButton = std::make_shared<flc_GUI::Button> (*m_context.m_fonts, *m_context.m_textures);
  introButton->setPosition (FULL_BOARD_SIZE_WIDTH + 5, APP_HEIGHT - introButton->getLocalBounds().height - 5);
  introButton->setText (L"Intro.");
  introButton->setCallback ([this]()
  {
    std::cout << "Intro button callback" << std::endl;
    m_stateManager->setState (State::StateType::Intro);
  });
  m_GUIContainerPositioning.pack (introButton);

  // Cancel game button ********************************************************
  auto cancelGameButton = std::make_shared<flc_GUI::Button> (*m_context.m_fonts, *m_context.m_textures);
  cancelGameButton->setPosition (FULL_BOARD_SIZE_WIDTH - cancelGameButton->getLocalBounds().width / 2, APP_HEIGHT - cancelGameButton->getLocalBounds().height - 5);
  cancelGameButton->setText (L"Annuler");
  cancelGameButton->setCallback ([this]()
  {
    std::cout << "Cancel button callback" << std::endl;
    m_stateManager->setState (State::StateType::NewGame);
  });
  m_GUIContainerGameRunning.pack (cancelGameButton);

  // New game button ***********************************************************
  auto newGameButton = std::make_shared<flc_GUI::Button> (*m_context.m_fonts, *m_context.m_textures);
  newGameButton->setPosition (FULL_BOARD_SIZE_WIDTH - newGameButton->getLocalBounds().width / 2, BOARD_SIZE / 2 + newGameButton->getLocalBounds().height - 5);
  newGameButton->setText (L"Rejouer");
  newGameButton->setCallback ([this]()
  {
    std::cout << "New game button callback" << std::endl;
    m_stateManager->setState (State::StateType::NewGame);
  });
  m_GUIContainerGameOver.pack (newGameButton);

  // Intro button **************************************************************
  auto introButtonGameOver = std::make_shared<flc_GUI::Button> (*m_context.m_fonts, *m_context.m_textures);
  introButtonGameOver->setPosition (newGameButton->getGlobalBounds().left, newGameButton->getGlobalBounds().top + newGameButton->getLocalBounds().height + 5);
  introButtonGameOver->setText (L"Intro.");
  introButtonGameOver->setCallback ([this]()
  {
    std::cout << "Intro button callback" << std::endl;
    m_stateManager->setState (State::StateType::Intro);
  });
  m_GUIContainerGameOver.pack (introButtonGameOver);

  // Player 1 missiles text ****************************************************
  m_P1MissilesText.setFont (m_context.m_fonts->get (Fonts::Normal));
  m_P1MissilesText.setString (m_missilesTextMessage + L"0/" + std::to_wstring (NUM_MISSILES));
  m_P1MissilesText.setCharacterSize (22);
  //m_P1MissilesText.setFillColor (sf::Color (0, 176, 238));
  m_P1MissilesText.setFillColor (sf::Color::Black);
  m_P1MissilesText.setPosition (P1_MISSILES_TEXT_POSX, P1_MISSILES_TEXT_POSY);

  // Game over text ************************************************************
  m_GameOverText.setFont (m_context.m_fonts->get (Fonts::GameOver));
  m_GameOverText.setString ("");
  m_GameOverText.setCharacterSize (100);
  m_GameOverText.setFillColor (sf::Color::White);
  m_GameOverText.setPosition (FULL_BOARD_SIZE_WIDTH, BOARD_SIZE / 2);

  // Create players' boards ****************************************************
  m_P1Board.getSprite().setTexture (m_context.m_textures->get (Textures::BoardBlue));
  m_P1Board.getSprite().setPosition (0.f, 0.f);
  m_P1Board.getBattlefield().setBounds (sf::IntRect (P1_BOARD_POSX, P1_BOARD_POSY, BOARD_SIZE, BOARD_SIZE), NUM_COLS_AND_ROWS, NUM_COLS_AND_ROWS, TILE_SIZE);
  m_P1Board.setMarginSize (TILE_SIZE);

  m_P2Board.getSprite().setTexture (m_context.m_textures->get (Textures::BoardOrange));
  m_P2Board.getSprite().setPosition (FULL_BOARD_SIZE_WIDTH, 0.f);
  m_P2Board.getBattlefield().setBounds (sf::IntRect (P2_BOARD_POSX, P2_BOARD_POSY, BOARD_SIZE, BOARD_SIZE), NUM_COLS_AND_ROWS, NUM_COLS_AND_ROWS, TILE_SIZE);
  m_P2Board.setMarginSize (TILE_SIZE);

  // Create boats and missiles
  createBoats (m_P1Boats, NUM_BOATS, *m_context.m_textures);
  for (unsigned int i = 0; i < NUM_BOATS; i++)
  {
    m_P1Boats[i]->getSprite().setPosition (P1_BOARD_POSX, P1_BOARD_POSY + i % NUM_BOATS * P1_BOARD_POSY); // initial position
    m_P1Boats[i]->updateOccupiedCells (m_P1Board.getBattlefield()); // update position on battlefield
    std::cout << "Player1 " << m_P1Boats[i]->occupiedCellsAsString() << std::endl;
  }

  createBoats (m_P2Boats, NUM_BOATS, *m_context.m_textures);
  boatsAutoPlacement (m_P2Boats, m_P2Board); // Create AI/Player2 fleet disposition
  for (auto &boat : m_P2Boats)
  {
    std::cout << "Player2 " << boat->occupiedCellsAsString() << std::endl;
  }

  createMissiles (m_P1Missiles, NUM_MISSILES, *m_context.m_textures, TILE_SIZE);
  std::cout << "Player1 #missiles: " << m_P1Missiles.size() << std::endl;
  createMissiles (m_P2Missiles, NUM_MISSILES, *m_context.m_textures, TILE_SIZE);
  std::cout << "Player2 #missiles: " << m_P2Missiles.size() << std::endl;
  for (unsigned int i = 0; i < NUM_MISSILES; i++)
  {
    m_P1Missiles[i]->getSprite().setPosition (P1_MISSILES_POSX, P1_MISSILES_POSY); // initial position
    m_P2Missiles[i]->getSprite().setPosition (P2_MISSILES_POSX, P2_MISSILES_POSY);
  }

  // Init Player2 missiles targets and missile firering order
  for (unsigned int i = 0; i < NUM_MISSILES; i++)
  {
    m_P2Missiles[i]->setTargetRowCol (i / NUM_COLS_AND_ROWS, i % NUM_COLS_AND_ROWS);
  }
  initMissileOrderList();

  // Music theme
  m_context.m_musics->play (Music::GameTheme);
}

GameState::~GameState()
{
  //dtor
}

void GameState::processEvents (const sf::Event &e)
{
  if (e.type == sf::Event::GainedFocus)
  {
    if ((m_gameState == GameStates::BoatsPositioning) || (m_gameState == GameStates::GameOver))
      m_context.m_musics->setPaused (false);
    m_isPaused = false;
  }
  else if (e.type == sf::Event::LostFocus)
  {
    if ((m_gameState == GameStates::BoatsPositioning) || (m_gameState == GameStates::GameOver))
      m_context.m_musics->setPaused (true);
    m_isPaused = true;
  }

  m_currentMousePosition = sf::Mouse::getPosition (*m_context.m_window);
  switch (e.type)
  {
  case sf::Event::MouseButtonPressed:
    handlePlayerMouse (e.mouseButton, m_currentMousePosition, true);
    break;
  case sf::Event::MouseButtonReleased:
    handlePlayerMouse (e.mouseButton, m_currentMousePosition, false);
    break;
  case sf::Event::MouseWheelMoved:
  case sf::Event::MouseWheelScrolled:
  case sf::Event::MouseEntered:
  case sf::Event::MouseLeft:
  case sf::Event::MouseMoved:
  case sf::Event::TouchBegan:
  case sf::Event::TouchMoved:
  case sf::Event::TouchEnded:
  case sf::Event::JoystickButtonPressed:
  case sf::Event::JoystickButtonReleased:
  case sf::Event::JoystickMoved:
  case sf::Event::JoystickConnected:
  case sf::Event::JoystickDisconnected:
  case sf::Event::Resized:
  case sf::Event::LostFocus:
  case sf::Event::GainedFocus:
  case sf::Event::TextEntered:
  case sf::Event::KeyPressed:
  case sf::Event::KeyReleased:
  case sf::Event::SensorChanged:
  case sf::Event::Count:
    break;
  case sf::Event::Closed:
    m_context.m_window->close();
    break;
  }

  if (m_gameState == GameStates::BoatsPositioning)
    m_GUIContainerPositioning.handleEvent (e);
  else if (m_gameState == GameStates::Running)
    m_GUIContainerGameRunning.handleEvent (e);
  else if (m_gameState == GameStates::GameOver)
    m_GUIContainerGameOver.handleEvent (e);
}

void GameState::update (sf::Time &dt)
{
  if (m_isPaused)
    return;

  switch (m_gameState)
  {
  case GameStates::BoatsPositioning:
    // Boat dynamic and final positions
    if (m_isBoatMove)
    {
      // Dynamically update boat position
      m_P1Boats[m_P1BoatId]->getSprite().setPosition (m_currentMousePosition.x - m_dx, m_currentMousePosition.y - m_dy);
    }
    else
    {
      if (m_P1BoatOldPosition.x != m_P1BoatNewPosition.x || m_P1BoatOldPosition.y != m_P1BoatNewPosition.y)
      {
        m_P1Boats[m_P1BoatId]->getSprite().setPosition (m_P1BoatNewPosition);
        // Check for collisions with other boats
        // If there is a collision the boat get back to his old position
        if (checkCollision<Boat> (m_P1BoatId, m_P1Boats) || checkMarginCollision (m_P1Boats[m_P1BoatId], m_P1Board))
        {
          m_P1Boats[m_P1BoatId]->getSprite().setPosition (m_P1BoatOldPosition);
          m_P1BoatNewPosition = m_P1BoatOldPosition;
        }
        else
        {
          m_P1BoatOldPosition = m_P1BoatNewPosition;
        }
        m_P1Boats[m_P1BoatId]->updateOccupiedCells (m_P1Board.getBattlefield()); // update occupied cells on battlefield
        std::cout << "Player1 " << m_P1Boats[m_P1BoatId]->occupiedCellsAsString() << std::endl;

        // Reset boat ID
        m_P1BoatId = -1;
      }
    }

    if (m_isBoatRotation)
    {
      m_P1Boats[m_P1BoatId]->rotate (TILE_SIZE);
      m_P1Boats[m_P1BoatId]->updateOccupiedCells (m_P1Board.getBattlefield()); // update occupied cells on battlefield
      std::cout << "Player1 " << m_P1Boats[m_P1BoatId]->occupiedCellsAsString() << std::endl;
      m_isBoatRotation = false; // frame rate issue
    }
    break;
  case GameStates::Running:
    if (m_isMissileMove)
    {
      m_isStartTextDisplayed = true; // display only once start game message on Player2 board

      if (m_playerTurn == PlayerTurn::Player1)
      {
        m_P1Missiles[m_P1MissileId]->getSprite().setPosition (m_P1MissileNewPosition);
        m_P1Missiles[m_P1MissileId]->getExplosionAnimation().setPosition (m_P1MissileNewPosition);
        m_P1Missiles[m_P1MissileId]->getFireAnimation().setPosition (m_P1MissileNewPosition);
        m_P1MissileId++;
        for (int i = 0; i < NUM_MISSILES; i++)
        {
          //std::cout << "P1 Missile id #" << i << " status: " << m_P1Missiles[i]->getStatus() << "||";
        }
        std::cout << std::endl;
        m_playerTurn = PlayerTurn::Player2;
      }
      else if (m_playerTurn == PlayerTurn::Player2)
      {
        m_P2Missiles[m_P2MissileId]->getSprite().setPosition (m_P2MissileNewPosition);
        m_P2Missiles[m_P2MissileId]->getExplosionAnimation().setPosition (m_P2MissileNewPosition);
        m_P2Missiles[m_P2MissileId]->getFireAnimation().setPosition (m_P2MissileNewPosition);
        m_playerTurn = PlayerTurn::Player1;
      }
      m_isMissileMove = false;
    }

    updateMissilesAnimation (dt);

    if (m_P1GlobalHealth == 0 || m_P2GlobalHealth == 0)
    {
      m_context.m_musics->play (Music::MenuTheme);
      m_gameState = GameStates::GameOver;
    }
    break;
  case GameStates::GameOver:
    updateMissilesAnimation (dt);

    m_duration += dt.asMilliseconds();

    if (m_duration > 1000)
    {
      m_GameOverText.setFillColor (sf::Color::White);
    }
    if (m_duration > 2000)
    {
      m_GameOverText.setFillColor (sf::Color::Yellow);
      m_duration = 0;
    }
    break;
  }
}

void GameState::render()
{
  m_context.m_window->clear (sf::Color::White);

  m_context.m_window->draw (m_BoardBackgroundSprite);
  m_context.m_window->draw (m_P1Board.getSprite());
  m_context.m_window->draw (m_P2Board.getSprite());

  for (int i = 0; i < NUM_BOATS; i++)
    m_context.m_window->draw (m_P1Boats[i]->getSprite());

  for (int i = 0; i < NUM_BOATS; i++)
    if (m_P2Boats[i]->getHealth() == 0)
      m_context.m_window->draw (m_P2Boats[i]->getSprite());

  for (int i = 0; i < NUM_MISSILES; i++)
  {
    if (m_P1Missiles[i]->getStatus() == Missile::Status::Miss)
      m_context.m_window->draw (m_P1Missiles[i]->getSprite());
    else if (m_P1Missiles[i]->getStatus() == Missile::Status::Hit)
    {
      if (!m_P1Missiles[i]->getExplosionAnimation().isFinished())
        m_context.m_window->draw (m_P1Missiles[i]->getExplosionAnimation());
      else
        m_context.m_window->draw (m_P1Missiles[i]->getFireAnimation());
    }
  }

  for (int i = 0; i < NUM_MISSILES; i++)
  {
    if (m_P2Missiles[i]->getStatus() == Missile::Status::Miss)
      m_context.m_window->draw (m_P2Missiles[i]->getSprite());
    else if (m_P2Missiles[i]->getStatus() == Missile::Status::Hit)
    {
      if (!m_P2Missiles[i]->getExplosionAnimation().isFinished())
        m_context.m_window->draw (m_P2Missiles[i]->getExplosionAnimation());
      else
        m_context.m_window->draw (m_P2Missiles[i]->getFireAnimation());
    }
  }

  m_context.m_window->draw (m_P1MissilesText);

  if (m_gameState == GameStates::BoatsPositioning)
  {
    m_context.m_window->draw (m_introSprite);
    m_context.m_window->draw (m_introText);
    m_context.m_window->draw (m_GUIContainerPositioning);
  }
  else if (m_gameState == GameStates::Running)
  {
    if (!m_isStartTextDisplayed)
    {
      m_context.m_window->draw (m_startText);
    }
    m_context.m_window->draw (m_GUIContainerGameRunning);
  }
  else if (m_gameState == GameStates::GameOver)
  {
    if (m_P1GlobalHealth == 0)
      m_GameOverText.setString (m_gameOverTextLooseStr);
    else if (m_P2GlobalHealth == 0)
      m_GameOverText.setString (m_gameOverTextWinStr);
    else
      m_GameOverText.setString ("!!! ERROR !!!");

    sf::FloatRect textRect = m_GameOverText.getLocalBounds();
    m_GameOverText.setOrigin (textRect.left + textRect.width / 2, textRect.top + textRect.height / 2);
    m_context.m_window->draw (m_GameOverText);

    m_context.m_window->draw (m_GUIContainerGameOver);
  }

  m_context.m_window->display();
}

void GameState::handlePlayerMouse (sf::Event::MouseButtonEvent mouseButtonEvent, sf::Vector2i &mousePosition, bool isButtonPressed)
{
  switch (m_gameState)
  {
  case GameStates::BoatsPositioning:
    if (mouseButtonEvent.button == sf::Mouse::Left)
    {
      if (isButtonPressed)
      {
        // Boat drag and drop initial position
        int id = getSelectedActor (m_P1Boats, mousePosition);
        if (id > -1)
        {
          m_isBoatMove = true;
          m_P1BoatId = id;
          m_P1BoatOldPosition = m_P1Boats[id]->getSprite().getPosition();
          m_dx = mousePosition.x - m_P1Boats[id]->getSprite().getPosition().x;
          m_dy = mousePosition.y - m_P1Boats[id]->getSprite().getPosition().y;
        }
      }
      else
      {
        m_isBoatMove = false;
        if (m_P1BoatId > -1)
        {
          // Boat drag and drop final posisiton
          sf::Vector2f p = m_P1Boats[m_P1BoatId]->getSprite().getPosition() + sf::Vector2f (TILE_SIZE / 2, TILE_SIZE / 2);
          m_P1BoatNewPosition = sf::Vector2f (TILE_SIZE * int (p.x / TILE_SIZE), TILE_SIZE * int (p.y / TILE_SIZE)); // snap to grid
        }
      }
    }

    if (mouseButtonEvent.button == sf::Mouse::Right)
    {
      if (isButtonPressed)
      {
        int id = getSelectedActor (m_P1Boats, mousePosition);
        if (id > -1)
        {
          // Rotate boat
          if (!checkBoatRotationCollision (id, m_P1Boats, m_P1Board))
          {
            m_isBoatRotation = true;
            m_P1BoatId = id;
          }
          else
            m_isBoatRotation = false;
        }
      }
      else
      {
        m_isBoatRotation = false;
      }
    }
    break;
  case GameStates::Running:
    if (m_playerTurn == PlayerTurn::Player1)
    {
      if (mouseButtonEvent.button == sf::Mouse::Left && isButtonPressed)
      {
        /////////////////////////////////////////
        // Player1 fires missile on Player2 board
        /////////////////////////////////////////
        sf::Vector2<int> clickPos = getClickedCell(); // get row/col indexes
        if (clickPos.x > -1 && clickPos.y > -1)
        {
          if (m_P1MissileId < NUM_MISSILES && m_P2Board.getBattlefield().getCell (clickPos.x, clickPos.y).getContent() == flc_GameBoard::Cell::ContentType::None)
          {
            m_isMissileMove = true;
            std::cout << "m_P1MissileId:" << m_P1MissileId << std::endl;
            m_P1MissilesText.setString (m_missilesTextMessage + std::to_wstring (m_P1MissileId + 1) + L"/" + std::to_wstring (NUM_MISSILES));
            std::cout << "Player1 clicks Cell[" << clickPos.x << "," << clickPos.y << "]" << std::endl;
            // Player2 sets Player1 missile position on his battlefield
            m_P1MissileNewPosition = m_P2Board.getBattlefield().getCell (clickPos.x, clickPos.y).getPosition();
            m_P2Board.getBattlefield().getCell (clickPos.x, clickPos.y).setContent (flc_GameBoard::Cell::ContentType::Miss);

            /* Player2 checks if one of its boats is concerned by Player1
               missile Player2.checkDamages(missilePosition) -> Hit or Miss */
            bool aBoatIsDamaged = false;
            for (int i = 0; i < NUM_BOATS; i++)
            {
              sf::Vector2i missileTarget = clickPos;
              if (m_P2Boats[i]->isHitted (missileTarget))
              {
                // Player2 updates concerned boat health
                std::cout << "Player1 hit " << m_P2Boats[i]->getName() << std::endl;
                m_P2Boats[i]->decrementHealth();
                m_P2GlobalHealth--;
                if (m_P2Boats[i]->getHealth() == 0)
                  std::cout << "Player1 destroyed " << m_P2Boats[i]->getName() << std::endl;
                // Player1 updates missile status (Hit or Miss)
                aBoatIsDamaged = true;
                break;
              }
            }

            if (aBoatIsDamaged)
            {
              m_P1Missiles[m_P1MissileId]->setStatus (Missile::Status::Hit);
              m_context.m_sounds->play (SoundEffects::Explosion5);
            }
            else
              m_P1Missiles[m_P1MissileId]->setStatus (Missile::Status::Miss);
          }
        }
      }
    }
    else if (m_playerTurn == PlayerTurn::Player2)
    {
      /////////////////////////////////////////
      // Player2 fires missile on Player1 board
      /////////////////////////////////////////
      if (!m_P2MissileOrderList.empty())
      {
        if (!m_P2HuntTargets.isEmpty())
        {
          m_P2MissileId = m_P2HuntTargets.getTargets().front();
          removeFromMissileOrderList (m_P2MissileId);
        }
        else
        {
          std::srand (unsigned (std::time (0)));
          std::random_shuffle (m_P2MissileOrderList.begin(), m_P2MissileOrderList.end());
          m_P2MissileId = m_P2MissileOrderList.front();
          m_P2MissileOrderList.pop_front();
        }

        /* Player2 asks Player1 battlefield object for the pixel position
           of a given cell from missile (row,col) target */
        sf::Vector2i missileTarget = m_P2Missiles[m_P2MissileId]->getTargetRowCol();
        std::cout << "Player2 clicks Cell[" << missileTarget.x << "," << missileTarget.y << "]" << std::endl;
        m_P2MissileNewPosition = m_P1Board.getBattlefield().getCell (missileTarget.x, missileTarget.y).getPosition();
        m_isMissileMove = true;

        /* Player1 checks if one of its boats is touched. If there is a Hit
           Player1 decrease the concerned boat health.
           If the boat health is zero, the boat is shown to Player2 */
        bool isBoatDestroyed = false;
        for (int i = 0; i < NUM_BOATS; i++)
        {
          /* Player1 informs Player2 of Player2 missile status (Hit/Miss)
             In case of a Hit, Player2 missile texture is changed accordingly */
          if (m_P1Boats[i]->isHitted (missileTarget))
          {
            std::cout << "Player2 hit " << m_P1Boats[i]->getName() << std::endl;
            m_P1Boats[i]->decrementHealth();
            m_P1GlobalHealth--;
            m_P2HuntMode = HuntMode::Intensive;

            // Update board cell status with Hit. Will be overriden if the boat is destroyed
            m_P1Board.getBattlefield().getCell (missileTarget.x, missileTarget.y).setContent (flc_GameBoard::Cell::ContentType::Hit);

            if (m_P1Boats[i]->getHealth() == 0)
            {
              std::cout << "Player2 destroyed " << m_P1Boats[i]->getName() << std::endl;
              /* The boat is destroyed, so Player1 must reveal it to Player2.
                 We update the concerned cells. */
              std::vector<flc_GameBoard::Cell*> cells = m_P1Boats[i]->getOccupiedCells();
              for (flc_GameBoard::Cell* cell : cells)
              {
                m_P1Board.getBattlefield().getCell (cell->getRowColl()).setContent (flc_GameBoard::Cell::ContentType::DestroyedBoat);
              }

              isBoatDestroyed = true;
            }
            m_P2Missiles[m_P2MissileId]->setStatus (Missile::Status::Hit);
            m_context.m_sounds->play (SoundEffects::Explosion7);
            break;
          }
          else
          {
            std::cout << "Player2 missed" << std::endl;
            m_P2Missiles[m_P2MissileId]->setStatus (Missile::Status::Miss);
          }
        }

        if (m_P2HuntMode == HuntMode::Normal)
        {
          std::cout << "Player2 normal hunt" << std::endl;
        }
        else if (m_P2HuntMode == HuntMode::Intensive)
        {
          if (m_P2HuntTargets.isEmpty())
          {
            std::cout << "Player2 starts intensive hunt..." << std::endl;
            initHuntTargets (m_P2MissileId);
          }
          else
          {
            if (m_P2Missiles[m_P2MissileId]->getStatus() == Missile::Status::Hit)
            {
              if (!isBoatDestroyed)
              {
                std::cout << "Player2 continue intensive hunt..." << std::endl;
                m_P2HuntTargets.removeChild (m_P2MissileId);
              }
              else
              {
                // Search for a damaged but not destroyed boat or switch to
                // normal mode
                int nextBoatId = searchHit();
                if (nextBoatId > -1)
                {
                  std::cout << "Player2 continue intensive hunt from left hit #:" << nextBoatId << std::endl;
                  m_P2HuntTargets.clear();
                  initHuntTargets (nextBoatId);
                }
                else
                {
                  std::cout << "Player2 switches to normal hunt" << std::endl;
                  m_P2HuntTargets.clear();
                  m_P2HuntMode = HuntMode::Normal;
                }
              }
            }
            else if (m_P2Missiles[m_P2MissileId]->getStatus() == Missile::Status::Miss)
            {
              std::cout << "Player2 removes useless children targets" << std::endl;
              m_P2HuntTargets.clearChildren (m_P2MissileId);
            }
          }
          std::cout << "Player2 hunt targets: " << m_P2HuntTargets.toString() << std::endl;
        }
      }
    }
    break;
  case GameStates::GameOver:
    if (m_P1GlobalHealth == 0)
      std::cout << "Player2 wins" << std::endl;
    else if (m_P2GlobalHealth == 0)
      std::cout << "Player1 wins" << std::endl;
    break;
  }
}

bool GameState::checkBoatRotationCollision (ActorId boatId, std::vector<std::unique_ptr<Boat>> &boats, flc_GameBoard::Board &board)
{
  bool collision = false;
  std::unique_ptr<Boat> rotatedBoat = std::unique_ptr<Boat> (new Boat (*boats[boatId]));
  rotatedBoat->rotate (TILE_SIZE);

  for (ActorId i = 0; i < boats.size(); i++)
  {
    if (i == boatId)
      continue;
    if (rotatedBoat->getSprite().getGlobalBounds().intersects (boats[i]->getSprite().getGlobalBounds()) || checkMarginCollision (rotatedBoat, board))
    {
      collision = true;
      break; // found a collision
    }
  }
  return collision;
}

bool GameState::checkMarginCollision (const std::unique_ptr<Boat> &actor, flc_GameBoard::Board &board)
{
  bool collision = false;
  sf::FloatRect actorBounds = actor->getSprite().getGlobalBounds();
  sf::FloatRect battlefieldBounds = static_cast<sf::FloatRect> (board.getBattlefield().getBounds());

  // Left margin collision
  if (actorBounds.left < battlefieldBounds.left)
    collision = true;
  // Right margin collision
  if ((actorBounds.left + actorBounds.width) > (battlefieldBounds.left + battlefieldBounds.width))
    collision = true;
  // Top margin collision
  if (actorBounds.top < battlefieldBounds.top)
    collision = true;
  // Bottom margin collision
  if ((actorBounds.top + actorBounds.height) > (battlefieldBounds.top + battlefieldBounds.height))
    collision = true;
  return collision;
}

sf::Vector2<int> GameState::getClickedCell()
{
  sf::Vector2<int> res (-1, -1);
  if (m_P2Board.getBattlefield().getBounds().contains (m_currentMousePosition))
  {
    for (unsigned int i = 0; i < m_P2Board.getBattlefield().getNumRows(); i++)
      for (unsigned int j = 0; j < m_P2Board.getBattlefield().getNumCols(); j++)
        if (m_P2Board.getBattlefield().getCell (i, j).getBounds().contains (m_currentMousePosition))
        {
          res.x = i;
          res.y = j;
        }
  }
  return res;
}

void GameState::initMissileOrderList()
{
  for (unsigned int i = 0; i < NUM_MISSILES; i++)
    m_P2MissileOrderList.push_back (i);

  std::srand (unsigned (std::time (0)));
  std::random_shuffle (m_P2MissileOrderList.begin(), m_P2MissileOrderList.end());
}

void GameState::removeFromMissileOrderList (unsigned int missileId)
{
  for (unsigned int i = 0; i < m_P2MissileOrderList.size(); i++)
  {
    if (m_P2MissileOrderList[i] == missileId)
      m_P2MissileOrderList.erase (m_P2MissileOrderList.begin() + i);
  }
}

// AI search for a damaged boat (with hits, not destroyed)
int GameState::searchHit()
{
  int result = -1;
  unsigned int nRows = m_P1Board.getBattlefield().getNumRows();
  unsigned int nCols = m_P1Board.getBattlefield().getNumCols();
  for (unsigned int i = 0; i < nRows; i++)
  {
    for (unsigned int j = 0; j < nCols; j++)
    {
      if (m_P1Board.getBattlefield().getCell (i, j).getContent() == flc_GameBoard::Cell::ContentType::Hit)
      {
        result = rowColToIndex (i, j, NUM_COLS_AND_ROWS);
      }
    }
  }
  return result;
}

void GameState::initHuntTargets (unsigned int missileId)
{
  sf::Vector2i rowCol = indexToRowCol (missileId, NUM_COLS_AND_ROWS);
  int magnitude = 4;

  m_P2HuntTargets.setRoot (missileId);
  m_P2HuntTargets.clear();

  // Left and right targets
  int lowerBound = rowCol.y - magnitude >= 0 ? rowCol.y - magnitude : 0;
  int upperBound = rowCol.y + magnitude < NUM_COLS_AND_ROWS ? rowCol.y + magnitude : NUM_COLS_AND_ROWS - 1;

  for (int i = rowCol.y - 1; i >= lowerBound ; i--)
  {
    if (m_P1Board.getBattlefield().getCell (rowCol.x, i).getContent() == flc_GameBoard::Cell::ContentType::None)
    {
      m_P2HuntTargets.addChild (rowColToIndex (rowCol.x, i, NUM_COLS_AND_ROWS), HuntTargets::Directions::Left);
    }
    else
      break;
  }

  for (int i = rowCol.y + 1; i <= upperBound; i++)
  {
    if (m_P1Board.getBattlefield().getCell (rowCol.x, i).getContent() == flc_GameBoard::Cell::ContentType::None)
    {
      m_P2HuntTargets.addChild (rowColToIndex (rowCol.x, i, NUM_COLS_AND_ROWS), HuntTargets::Directions::Right);
    }
    else
      break;
  }

  // Up and down targets
  lowerBound = rowCol.x - magnitude >= 0 ? rowCol.x - magnitude : 0;
  upperBound = rowCol.x + magnitude < NUM_COLS_AND_ROWS ? rowCol.x + magnitude : NUM_COLS_AND_ROWS - 1;

  for (int i = rowCol.x - 1; i >= lowerBound ; i--)
  {
    if (m_P1Board.getBattlefield().getCell (i, rowCol.y).getContent() == flc_GameBoard::Cell::ContentType::None)
    {
      m_P2HuntTargets.addChild (rowColToIndex (i, rowCol.y, NUM_COLS_AND_ROWS), HuntTargets::Directions::Up);
    }
    else
      break;
  }

  for (int i = rowCol.x + 1; i <= upperBound; i++)
  {
    if (m_P1Board.getBattlefield().getCell (i, rowCol.y).getContent() == flc_GameBoard::Cell::ContentType::None)
    {
      m_P2HuntTargets.addChild (rowColToIndex (i, rowCol.y, NUM_COLS_AND_ROWS), HuntTargets::Directions::Down);
    }
    else
      break;
  }
}

//bool GameState::isUselessTarget (unsigned int missileId)
//{
//  bool res = false;
//  sf::Vector2i rowCol = indexToRowCol (missileId, NUM_COLS_AND_ROWS);
//
//  if (rowCol.x > 0 && rowCol.x < (NUM_COLS_AND_ROWS - 1) && rowCol.y > 0 && rowCol.y < (NUM_COLS_AND_ROWS - 1))
//  {
//    if (m_P1Board.getBattlefield().getCell (rowCol.x - 1, rowCol.y).getContent() != GameBoard::Cell::ContentType::None
//        && m_P1Board.getBattlefield().getCell (rowCol.x + 1, rowCol.y).getContent() != GameBoard::Cell::ContentType::None
//        && m_P1Board.getBattlefield().getCell (rowCol.x, rowCol.y - 1).getContent() != GameBoard::Cell::ContentType::None
//        && m_P1Board.getBattlefield().getCell (rowCol.x, rowCol.y + 1).getContent() != GameBoard::Cell::ContentType::None)
//      res = true;
//  }
//  return res;
//}
//
//int GameState::computeSpaceIndex (unsigned int missileId, int delta)
//{
//  int spaceIndex = 0;
//  sf::Vector2i rowCol = indexToRowCol (missileId, NUM_COLS_AND_ROWS);
//
//  int lowerBound;
//  if (rowCol.y - delta < 0)
//    lowerBound = 0;
//  else
//    lowerBound = rowCol.y - delta;
//  assert (lowerBound >= 0);
//
//  int upperBound;
//  if ((rowCol.y + delta) < NUM_COLS_AND_ROWS)
//    upperBound = rowCol.y + delta;
//  else
//    upperBound =  NUM_COLS_AND_ROWS;
//  assert (upperBound <= NUM_COLS_AND_ROWS);
//
//  for (int i = lowerBound; i < upperBound; i++)
//  {
//    if (m_P1Board.getBattlefield().getCell (rowCol.x, i).getContent() == GameBoard::Cell::ContentType::Miss)
//      spaceIndex--;
//  }
//
//  if ((rowCol.x - delta) < 0)
//    lowerBound = 0;
//  else
//    lowerBound = rowCol.x - delta;
//  assert (lowerBound >= 0);
//
//  if ((rowCol.x + delta) < NUM_COLS_AND_ROWS)
//    upperBound = rowCol.x + delta;
//  else
//    upperBound = NUM_COLS_AND_ROWS;
//  assert (upperBound <= NUM_COLS_AND_ROWS);
//
//  for (int i = lowerBound; i < upperBound; i++)
//  {
//    if (m_P1Board.getBattlefield().getCell (i, rowCol.y).getContent() == GameBoard::Cell::ContentType::Miss)
//      spaceIndex--;
//  }
//  return spaceIndex;
//}

void GameState::updateMissilesAnimation (sf::Time dt)
{
  for (unsigned int i = 0; i < NUM_MISSILES; i++)
  {
    if (m_P1Missiles[i]->getStatus() == Missile::Status::Hit && !m_P1Missiles[i]->getExplosionAnimation().isFinished())
      m_P1Missiles[i]->getExplosionAnimation().update (dt);
    if (m_P1Missiles[i]->getStatus() == Missile::Status::Hit && m_P1Missiles[i]->getExplosionAnimation().isFinished())
      m_P1Missiles[i]->getFireAnimation().update (dt);

    if (m_P2Missiles[i]->getStatus() == Missile::Status::Hit && !m_P2Missiles[i]->getExplosionAnimation().isFinished())
      m_P2Missiles[i]->getExplosionAnimation().update (dt);
    if (m_P2Missiles[i]->getStatus() == Missile::Status::Hit && m_P2Missiles[i]->getExplosionAnimation().isFinished())
      m_P2Missiles[i]->getFireAnimation().update (dt);
  }
}

void GameState::boatsAutoPlacement (std::vector<std::unique_ptr<Boat>> &boats, flc_GameBoard::Board &board)
{
  m_fleetGenerator.createFleet(); // Create a fleet dispostion
  boats.clear();
  createBoats (boats, NUM_BOATS, *m_context.m_textures);

  for (unsigned int i = 0; i < boats.size(); i++)
  {
    flc_FeetGenerator::ActorPosition actorPosition = m_fleetGenerator.getFleet().at (static_cast<Boat::BoatType> (i));
    flc_GameBoard::Cell cellPosition = board.getBattlefield().getCell (actorPosition.getRowsCols()[0]);

    boats[i]->getSprite().setPosition (cellPosition.getPosition());
    if (actorPosition.getRotation())
      boats[i]->rotate (TILE_SIZE);

    boats[i]->updateOccupiedCells (board.getBattlefield());
  }
}

void createBoats (std::vector<std::unique_ptr<Boat>> &boats, unsigned int numBoats, TextureHolder &textures)
{
  for (unsigned int i = 0; i < numBoats; i++)
  {
    std::unique_ptr<Boat> boat (new Boat);
    boat->setBoatType (Boat::BoatType (i));
    switch (boat->getBoatType())
    {
    case Boat::Destroyer:
      boat->getSprite().setTexture (textures.get (Textures::Destroyer));
      break;
    case Boat::Submarine:
      boat->getSprite().setTexture (textures.get (Textures::Submarine));
      break;
    case Boat::Cruiser:
      boat->getSprite().setTexture (textures.get (Textures::Cruiser));
      break;
    case Boat::Battleship:
      boat->getSprite().setTexture (textures.get (Textures::Battleship));
      break;
    case Boat::Carrier:
      boat->getSprite().setTexture (textures.get (Textures::Carrier));
      break;
    }
    boats.push_back (std::move (boat));
  }
}

void createMissiles (std::vector<std::unique_ptr<Missile>> &missiles, unsigned int numMissiles, TextureHolder &textures, unsigned int tileSize)
{
  for (unsigned int i = 0; i < numMissiles; i++)
  {
    std::unique_ptr<Missile> missile (new Missile (i));

    missile->getSprite().setTexture (textures.get (Textures::Missile));
    missile->getSprite().setTextureRect (sf::IntRect (0, 0, tileSize, tileSize));

    missile->getExplosionAnimation().setTexture (textures.get (Textures::MissileExplosion));
    missile->getExplosionAnimation().setFrameSize (sf::Vector2i (64, 64));
    missile->getExplosionAnimation().setNumFrames (16);
    missile->getExplosionAnimation().setDuration (sf::seconds (1));
    missile->getExplosionAnimation().setRepeating (false);

    missile->getFireAnimation().setTexture (textures.get (Textures::MissileFire));
    missile->getFireAnimation().setFrameSize (sf::Vector2i (64, 64));
    missile->getFireAnimation().setNumFrames (60);
    missile->getFireAnimation().setDuration (sf::seconds (1));
    missile->getFireAnimation().setRepeating (true);

    missiles.push_back (std::move (missile));
  }
}

int rowColToIndex (int row, int col, int colDim)
{
  return row * colDim + col;
}

int rowColToIndex (sf::Vector2i rowCol, int colDim)
{
  return rowColToIndex (rowCol.x, rowCol.y, colDim);
}

sf::Vector2i indexToRowCol (int index, int colDim)
{
  sf::Vector2i res (index / colDim, index % colDim);
  return res;
}

bool isValidRowCol (sf::Vector2i rowCol, sf::Vector2i rowBounds, sf::Vector2i colBounds)
{
  if (rowCol.x >= rowBounds.x && rowCol.x <= rowBounds.y && rowCol.y >= colBounds.x && rowCol.y <= colBounds.y)
    return true;
  else
    return false;
}

}
