//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "label.h"

namespace flc_GUI
{

Label::Label (const std::string & text, const FontHolder &fonts):
  m_text (text, fonts.get (Fonts::Normal), 16)
{
  //ctor
}

Label::Label (const std::wstring & text, const FontHolder &fonts):
  m_text (text, fonts.get (Fonts::Normal), 16)
{
  //ctor
}

Label::~Label()
{
  //dtor
}

void Label::draw (sf::RenderTarget& target, sf::RenderStates states) const
{
  states.transform *= getTransform();
  target.draw (m_text, states);
}

bool Label::isSelectable() const
{
  return false;
}

void Label::setText (const std::string& text)
{
  m_text.setString (text);
}

void Label::setText (const std::wstring& text)
{
  m_text.setString (text);
}

void Label::handleEvent (const sf::Event& event)
{

}

sf::FloatRect Label::getGlobalBounds() const
{
  return m_text.getGlobalBounds();
}

sf::FloatRect Label::getLocalBounds() const
{
  return m_text.getLocalBounds();
}

}
