//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef ACTORPOSITION_H
#define ACTORPOSITION_H

#include <SFML/Graphics.hpp>

namespace flc_FeetGenerator
{

class ActorPosition
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  ActorPosition (unsigned int row, unsigned int col, unsigned int length, bool rotation = false);
  virtual ~ActorPosition();
  std::vector<sf::Vector2i> &getRowsCols() { return m_RowsCols; } // get first coordonates
  bool getRotation() const { return m_rotation; }
  bool intersects (ActorPosition &other) const;
  std::string getActorPositionAsString() const;

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  std::vector<sf::Vector2i> m_RowsCols;
  bool m_rotation;
  // Methods -------------------------------------------------------------------
};

template<class T>
std::string vector2AsString (T v)
{
  std::string res = "";
  res += "(x:" + std::to_string (v.x) + "|y:" + std::to_string (v.y) + ")";
  return res;
}

}

#endif // ACTORPOSITION_H
