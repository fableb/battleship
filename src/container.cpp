//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "container.h"

namespace flc_GUI
{

Container::Container():
  m_children(),
  m_selectedChild{-1}
{
  //ctor
}

Container::~Container()
{
  //dtor
}

bool Container::isSelectable() const
{
  return false;
}

void Container::draw (sf::RenderTarget &target, sf::RenderStates states) const
{
  states.transform *= getTransform();
  for (const auto &child : m_children)
  {
    target.draw (*child, states);
  }
}

bool Container::hasSelection() const
{
  return m_selectedChild >= 0;
}

void Container::select (std::size_t idx)
{
  if (m_children[idx]->isSelectable())
  {
    if (hasSelection())
      m_children[m_selectedChild]->deselect();
    m_children[idx]->select();
    m_selectedChild = idx;
  }
}

void Container::selectNext()
{
  if (!hasSelection())
    return;

  // Search for next selectable component
  int next = m_selectedChild;
  do
    next = (next + 1) % m_children.size();
  while (!m_children[next]->isSelectable());

  // Select the component
  select (next);
}

void Container::selectPrevious()
{
  if (!hasSelection())
    return;

  // Search previous selectable component
  int prev = m_selectedChild;
  do
    prev = (prev + m_children.size() - 1) % m_children.size();
  while (!m_children[prev]->isSelectable());

  // select the component
  select (prev);
}

void Container::pack (std::shared_ptr<Component> component)
{
  m_children.push_back (component);
  if (!hasSelection() && component->isSelectable())
    select (m_children.size() - 1);
}

void Container::handleEvent (const sf::Event& event)
{
  // Give an active child its events
  if (hasSelection() && m_children[m_selectedChild]->isActive())
  {
    m_children[m_selectedChild]->handleEvent (event);
  }
  else // Container handle events
  {
    if (event.type == sf::Event::KeyReleased)
    {
      if (event.key.code == sf::Keyboard::W || event.key.code == sf::Keyboard::Up)
      {
        selectPrevious();
      }
      else if (event.key.code == sf::Keyboard::S || event.key.code == sf::Keyboard::Down)
      {
        selectNext();
      }
      else if (event.key.code == sf::Keyboard::Return || event.key.code == sf::Keyboard::Space)
      {
        if (hasSelection())
          m_children[m_selectedChild]->activate();
      }
    }

    if (event.type == sf::Event::MouseButtonPressed)
    {
      if (event.mouseButton.button == sf::Mouse::Left)
      {
        if (hasSelection())
        {
          for (unsigned int i = 0; i < m_children.size(); i++)
          {
            if (m_children[i]->getGlobalBounds().contains (event.mouseButton.x, event.mouseButton.y))
            {
              // Deselect previous selected button
              if (m_selectedChild >= 0)
                m_children[m_selectedChild]->deselect();
              m_selectedChild = i;
              select (m_selectedChild);
              break;
            }
          }
        }
      }
    }

    if (event.type == sf::Event::MouseButtonReleased)
    {
      if (event.mouseButton.button == sf::Mouse::Left)
      {
        if (hasSelection())
        {
          for (unsigned int i = 0; i < m_children.size(); i++)
          {
            if (m_children[i]->getGlobalBounds().contains (event.mouseButton.x, event.mouseButton.y))
            {
              m_selectedChild = i;
              m_children[m_selectedChild]->activate();
              break;
            }
          }
        }
      }
    }
  }
}

sf::FloatRect Container::getGlobalBounds() const
{
  return sf::FloatRect (0, 0, 0, 0);
}

sf::FloatRect Container::getLocalBounds() const
{
  return sf::FloatRect (0, 0, 0, 0);
}

}
