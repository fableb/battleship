//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef BUTTON_H
#define BUTTON_H

#include <string>
#include <functional>
#include "component.h"
#include "resourceholder.h"
#include "utility.h"

namespace flc_GUI
{

class Button : public Component
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  Button (const FontHolder &fonts, const TextureHolder &textures);
  virtual ~Button();

  void setCallback (std::function<void()> callback);
  void setText (const std::string &text);
  void setText (const std::wstring &text);
  void setTextColor (sf::Color color);
  void setToggle (bool flag);

  virtual bool isSelectable() const;
  virtual void select();
  virtual void deselect();
  virtual void activate();
  virtual void deactivate();
  virtual void handleEvent (const sf::Event &event);
  virtual sf::FloatRect getGlobalBounds() const;
  virtual sf::FloatRect getLocalBounds() const;
  virtual void setPosition (const sf::Vector2f &position);
  virtual void setPosition (float x, float y);

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  std::function<void()> m_callback;
  const sf::Texture &m_normalTexture;
  const sf::Texture &m_selectedTexture;
  const sf::Texture &m_pressedTexture;
  sf::Sprite m_sprite;
  sf::Text m_text;
  bool m_isToggle;

  // Methods -------------------------------------------------------------------
  virtual void draw (sf::RenderTarget &target, sf::RenderStates states) const;
};

}

#endif // BUTTON_H
