//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef LABEL_H
#define LABEL_H

#include "component.h"
#include "resourceholder.h"

namespace flc_GUI
{

class Label : public Component
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  Label (const std::string & text, const FontHolder &fonts);
  Label (const std::wstring & text, const FontHolder &fonts);
  virtual ~Label();

  virtual bool isSelectable() const;
  void setText (const std::string &text);
  void setText (const std::wstring &text);
  virtual void handleEvent (const sf::Event &event);
  virtual sf::FloatRect getGlobalBounds() const;
  virtual sf::FloatRect getLocalBounds() const;

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  sf::Text m_text;
  // Methods -------------------------------------------------------------------
  void draw (sf::RenderTarget &target, sf::RenderStates states) const;
};

}

#endif // LABEL_H
