//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "boat.h"

Boat::Boat() :
  m_type{Destroyer},
  m_health{0},
  m_rotation{false}
{

}

Boat::Boat (const Boat &boat)
{
  m_id = boat.m_id;
  m_sprite = boat.m_sprite;
  m_type = boat.m_type;
  m_health = boat.m_health;
  m_rotation = boat.m_rotation;
}

Boat::~Boat()
{

}

void Boat::rotate (unsigned int tileSize)
{
  if (m_rotation)
  {
    m_sprite.rotate (90);
    sf::Vector2f pos = m_sprite.getPosition();
    pos.y -= tileSize;
    m_sprite.setPosition (pos);
    m_rotation = false;
  }
  else
  {
    m_sprite.rotate (-90);
    sf::Vector2f pos = m_sprite.getPosition();
    pos.y += tileSize;
    m_sprite.setPosition (pos);
    m_rotation = true;
  }
}

void Boat::setBoatType (const BoatType v)
{
  m_type = v;
  switch (m_type)
  {
  case Destroyer:
    m_health = DestroyerHealth;
    break;
  case Submarine:
    m_health = SubmarineHealth;
    break;
  case Cruiser:
    m_health = CruiserHealth;
    break;
  case Battleship:
    m_health = BattleshipHealth;
    break;
  case Carrier:
    m_health = CarrierHealth;
    break;
  default:
    m_health = 0;
  }
}

std::string Boat::getName()
{
  return m_names[m_type];
}

void Boat::updateOccupiedCells (flc_GameBoard::Battlefield &b)
{
  // Reset
  m_occupiedCells.clear();

  // Update occupied cells on battlefield
  for (unsigned int i = 0; i < b.getNumRows(); i++)
  {
    for (unsigned int j = 0; j < b.getNumCols(); j++)
    {
      sf::FloatRect cellBounds = static_cast<sf::FloatRect> (b.getCell (i, j).getBounds());
      if (m_sprite.getGlobalBounds().intersects (cellBounds))
      {
        m_occupiedCells.push_back (&b.getCell (i, j));
      }
    }
  }
}

std::string Boat::occupiedCellsAsString()
{
  std::string res = this->getName() + ":";
  for (unsigned int i = 0 ; i < m_occupiedCells.size(); i++)
  {
    res += "RowCol[" + std::to_string (m_occupiedCells[i]->getRowColl().x) + "," + std::to_string (m_occupiedCells[i]->getRowColl().y) + "] ";
  }
  return res;
}

bool Boat::isHitted (sf::Vector2i targetRowCol)
{
  bool result = false;
  for (unsigned int i = 0; i < m_occupiedCells.size(); i++)
  {
    if (m_occupiedCells[i]->getRowColl() == targetRowCol)
    {
      result = true;
      break;
    }
  }
  return result;
}
