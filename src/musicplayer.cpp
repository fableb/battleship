//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "musicplayer.h"

MusicPlayer::MusicPlayer():
  m_music(),
  m_filenames(),
  m_volume (50.f)
{
  m_filenames[Music::MenuTheme] = "assets/musics/bensound-epic.ogg";
  m_filenames[Music::GameTheme] = "assets/musics/bensound-ofeliasdream.ogg";
}

MusicPlayer::~MusicPlayer()
{
  //dtor
}
void MusicPlayer::play (Music::ID theme)
{
  std::string filename = m_filenames[theme];
  if (!m_music.openFromFile (filename))
    throw std::runtime_error ("Music " + filename + " could not be loaded.");

  m_music.setVolume (m_volume);
  m_music.setLoop (true);
  m_music.play();
}

void MusicPlayer::stop()
{
  m_music.stop();
}

void MusicPlayer::setPaused (bool paused)
{
  if (paused)
    m_music.pause();
  else
    m_music.play();
}

void MusicPlayer::setVolume (float volume)
{
  m_volume = volume;
}


