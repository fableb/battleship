//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Graphics.hpp>
#include "globals.h"
#include "board.h"
#include "boat.h"
#include "missile.h"

class Player
{
public:
  // Attributes ----------------------------------------------------------------
  enum PlayerId
  {
    Player1,
    Player2,
    Unknown
  };
  // Methods -------------------------------------------------------------------
  explicit Player (sf::RenderWindow &window);
  virtual ~Player();
  void update();
  void draw();
  PlayerId getId() const { return m_id; }
  void setId (PlayerId id) { m_id = id; }
  void setupGame();

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
private:
  // Attributes ----------------------------------------------------------------
  sf::RenderWindow &m_window;
  PlayerId m_id; // player id
  flc_GameBoard::Board m_board; // player boat
  std::unique_ptr<Boat> m_boats[NUM_BOATS]; // player fleet
  sf::Vector2i m_boatsPositions[NUM_BOATS]; // boats initial positions (x,y) for human players, final boat position for computer player
  std::unique_ptr<Missile> m_missiles[NUM_MISSILES]; // player missiles
  bool m_isBoatMove;
  bool m_isBoatRotation;
  bool m_isMissileMove;
  // Methods -------------------------------------------------------------------
};

#endif // PLAYER_H
