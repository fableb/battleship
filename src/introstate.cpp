//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "introstate.h"
#include "statemanager.h"

namespace flc_game
{

IntroState::IntroState (StateManager *stateManager, ApplicationContext context):
  State (stateManager, context),
  m_isPaused{false},
  m_GUIContainer()
{
  // Background
  m_sprite.setTexture (m_context.m_textures->get (Textures::Intro));

  // Play button
  auto playButton = std::make_shared<flc_GUI::Button> (*m_context.m_fonts, *m_context.m_textures);
  playButton->setPosition (m_sprite.getLocalBounds().width - playButton->getLocalBounds().width - 32, m_sprite.getLocalBounds().height -  playButton->getLocalBounds().height - 32);
  playButton->setText (m_playButtonTextStr);
  playButton->setCallback ([this]()
  {
    m_context.m_musics->stop();
    std::cout << "Switch to GameState" << std::endl;
    m_stateManager->setState (State::StateType::NewGame);
  });
  m_GUIContainer.pack (playButton);

  // Music theme
  m_context.m_musics->play (Music::MenuTheme);
}

IntroState::~IntroState()
{
  //dtor
}

void IntroState::update (sf::Time& dt)
{
  if (m_isPaused)
    return;
}

void IntroState::processEvents (const sf::Event& e)
{
  m_GUIContainer.handleEvent (e);

  if (e.type == sf::Event::GainedFocus)
  {
    m_context.m_musics->setPaused (false);
    m_isPaused = false;
  }
  else if (e.type == sf::Event::LostFocus)
  {
    m_context.m_musics->setPaused (true);
    m_isPaused = true;
  }

  m_currentMousePosition = sf::Mouse::getPosition (*m_context.m_window);
  switch (e.type)
  {
  case sf::Event::MouseButtonPressed:
  case sf::Event::MouseButtonReleased:
  case sf::Event::MouseWheelMoved:
  case sf::Event::MouseWheelScrolled:
  case sf::Event::MouseEntered:
  case sf::Event::MouseLeft:
  case sf::Event::MouseMoved:
  case sf::Event::TouchBegan:
  case sf::Event::TouchMoved:
  case sf::Event::TouchEnded:
  case sf::Event::JoystickButtonPressed:
  case sf::Event::JoystickButtonReleased:
  case sf::Event::JoystickMoved:
  case sf::Event::JoystickConnected:
  case sf::Event::JoystickDisconnected:
  case sf::Event::Resized:
  case sf::Event::LostFocus:
  case sf::Event::GainedFocus:
  case sf::Event::TextEntered:
  case sf::Event::KeyPressed:
  case sf::Event::KeyReleased:
  case sf::Event::SensorChanged:
  case sf::Event::Count:
    break;
  case sf::Event::Closed:
    m_context.m_window->close();
    break;
  }
}

void IntroState::render()
{
  m_context.m_window->clear (sf::Color::Black);
  m_context.m_window->draw (m_sprite);
  m_context.m_window->draw (m_GUIContainer);
  m_context.m_window->display();
}

}

