//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef RESOURCEHOLDER_H
#define RESOURCEHOLDER_H

#include <map>
#include <string>
#include <memory>
#include <cassert>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

namespace Textures
{
enum ID
{
  Intro,
  BoardBackground,
  BoardBlue,
  BoardOrange,
  ForegroundBlue,
  ButtonOrange0,
  ButtonOrange1,
  ButtonOrange2,
  Destroyer,
  Submarine,
  Cruiser,
  Battleship,
  Carrier,
  Missile,
  MissileExplosion,
  MissileFire
};
}

namespace Fonts
{
enum ID
{
  Normal,
  GameOver
};
}

namespace SoundEffects
{
enum ID
{
  Explosion5,
  Explosion7
};
}

template <typename Resource, typename Identifier>
class ResourceHolder
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  void load (Identifier id, const std::string &filename);
  template <typename Parameter>
  void load (Identifier id, const std::string &filename, const Parameter &secondParam);

  Resource &get (Identifier id);
  const Resource &get (Identifier id) const;

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  std::map<Identifier, std::unique_ptr<Resource>> m_resourceMap;
  // Methods -------------------------------------------------------------------
  void insertResource (Identifier id, std::unique_ptr<Resource> resource);
};

#include "resourceholder.inl"

typedef ResourceHolder<sf::Texture, Textures::ID> TextureHolder;
typedef ResourceHolder<sf::Font, Fonts::ID> FontHolder;
typedef ResourceHolder<sf::SoundBuffer, SoundEffects::ID> SoundBufferHolder;

#endif // RESOURCEHOLDER_H

