//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef GLOBALS_H_INCLUDED
#define GLOBALS_H_INCLUDED

const int NUM_FRAMES = 25;
const int TILE_SIZE = 64;
const int NUM_COLS_AND_ROWS = 10;
const int BOARD_SIZE = TILE_SIZE * NUM_COLS_AND_ROWS;
const int BOARD_MARGIN = TILE_SIZE;
const int FULL_BOARD_SIZE_WIDTH = BOARD_SIZE + BOARD_MARGIN;
const int FULL_BOARD_SIZE_HEIGHT = BOARD_SIZE + BOARD_MARGIN;
const int P1_BOARD_POSX = BOARD_MARGIN;
const int P1_BOARD_POSY = BOARD_MARGIN;
const int P2_BOARD_POSX = FULL_BOARD_SIZE_WIDTH + BOARD_MARGIN;
const int P2_BOARD_POSY = BOARD_MARGIN;
const int APP_WIDTH = FULL_BOARD_SIZE_WIDTH * 2;
const int APP_HEIGHT = FULL_BOARD_SIZE_WIDTH + BOARD_MARGIN;
const int NUM_BOATS = 5;
const int NUM_MISSILES = NUM_COLS_AND_ROWS * NUM_COLS_AND_ROWS;
const int P1_MISSILES_POSX = P1_BOARD_POSX;
const int P1_MISSILES_POSY = P1_BOARD_POSY + BOARD_SIZE;
const int P1_MISSILES_TEXT_POSX = P1_MISSILES_POSX;
const int P1_MISSILES_TEXT_POSY = P1_MISSILES_POSY + (TILE_SIZE / 2);
const int P2_MISSILES_POSX = P1_BOARD_POSX; // hide Player2 missiles
const int P2_MISSILES_POSY = P1_BOARD_POSY + BOARD_SIZE; // hide Player2 missiles
const int INTRO_TEXT_POSX = P2_BOARD_POSX;
const int INTRO_TEXT_POSY = P2_BOARD_POSY;
const int INTRO_TEXT_SIZE = 16;
const int P1_BUTTON_POSX = P2_BOARD_POSX;
const int P1_BUTTON_POSY = P2_BOARD_POSY;
const int CANCEL_BUTTON_POSX = 100;
const int CANCEL_BUTTON_POSY = 100;

#endif // GLOBALS_H_INCLUDED
