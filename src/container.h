//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef CONTAINER_H
#define CONTAINER_H

#include <vector>
#include <memory>
#include "component.h"

namespace flc_GUI
{

class Container : public Component
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  Container();
  virtual ~Container();
  void pack (std::shared_ptr<Component> component);
  virtual bool isSelectable() const;
  virtual void handleEvent (const sf::Event &event);
  virtual sf::FloatRect getGlobalBounds() const;
  virtual sf::FloatRect getLocalBounds() const;

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  std::vector<std::shared_ptr<Component>> m_children;
  int m_selectedChild;
  // Methods -------------------------------------------------------------------
  virtual void draw (sf::RenderTarget &target, sf::RenderStates states) const;
  bool hasSelection() const;
  void select (std::size_t idx);
  void selectNext();
  void selectPrevious();
};

}

#endif // CONTAINER_H
