//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef FLEETGENARATOR_H
#define FLEETGENARATOR_H

#include <cassert>
#include <map>
#include <vector>
#include <iostream>
#include <iomanip>
#include "actorposition.h"
#include "boat.h"

namespace flc_FeetGenerator
{

class FleetGenarator
{
public:
  // Attributes ----------------------------------------------------------------
  enum BoatWidth
  {
    Destroyer = 2,
    Submarine = 3,
    Cruiser = 3,
    Battleship = 4,
    Carrier = 5
  };
  // Methods -------------------------------------------------------------------
  explicit FleetGenarator (unsigned int rows, unsigned int cols);
  virtual ~FleetGenarator();
  unsigned int getBattlefieldRows() const { return m_rows; }
  unsigned int getBattlefieldCols() const { return m_cols; }
  void createFleet();
  void destroyFleet() { m_fleet.clear(); };
  std::map<Boat::BoatType, ActorPosition> const &getFleet() { return m_fleet; }

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  unsigned int m_rows; // battlefield number of rows
  unsigned int m_cols; // battlefield number of columns
  std::map<Boat::BoatType, ActorPosition> m_fleet; // position of each type of boats on battlefield
  std::vector<ActorPosition> m_destroyerPositions; // all possible positions on battlefield
  std::vector<ActorPosition> m_submarinePositions;
  std::vector<ActorPosition> m_cruiserPositions;
  std::vector<ActorPosition> m_battleshipPositions;
  std::vector<ActorPosition> m_carrierPositions;
  // Methods -------------------------------------------------------------------
  void setupPositions(); // create all possible position for each type of boat
};

}

#endif // FLEETGENARATOR_H
