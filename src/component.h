//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef COMPONENT_H
#define COMPONENT_H

#include <SFML/Graphics.hpp>

namespace flc_GUI
{

class Component: public sf::Drawable, public sf::Transformable, private sf::NonCopyable
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  Component();
  virtual ~Component();
  // the object can be selected (i.e. button... Text is not selectable for example.
  virtual bool isSelectable() const = 0;
  bool isSelected() const;
  virtual void select();
  virtual void deselect();
  virtual bool isActive() const;
  virtual void activate();
  virtual void deactivate();
  virtual void handleEvent (const sf::Event &event) = 0;
  virtual sf::FloatRect getGlobalBounds() const = 0;
  virtual sf::FloatRect getLocalBounds() const = 0;
  virtual bool isVisible() const;
  virtual void setIsVisible (bool flag);

protected:
  // Attributes ----------------------------------------------------------------
  bool m_isVisible;
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  bool m_isSelected;
  bool m_isActive;
  // Methods -------------------------------------------------------------------
};

}

#endif // COMPONENT_H
