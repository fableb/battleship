//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "actorposition.h"

namespace flc_FeetGenerator
{

ActorPosition::ActorPosition (unsigned int row, unsigned int col, unsigned int length, bool rotation):
  m_rotation{false}
{
  if (rotation == false)
  {
    // horizontal
    for (unsigned int j = col; j < col + length; j++)
      m_RowsCols.push_back (sf::Vector2i (row, j));
    m_rotation = false;
  }
  else
  {
    // vertical
    for (unsigned int i = row; i < row + length; i++)
      m_RowsCols.push_back (sf::Vector2i (i, col));
    m_rotation = true;
  }
}

ActorPosition::~ActorPosition()
{
  //dtor
}

bool ActorPosition::intersects (ActorPosition &other) const
{
  for (unsigned int i = 0; i < m_RowsCols.size(); i++)
  {
    for (unsigned int j = 0; j < other.m_RowsCols.size(); j++)
    {
      if (m_RowsCols[i] == other.m_RowsCols[j])
        return true;
    }
  }
  return false;
}

std::string ActorPosition::getActorPositionAsString() const
{
  std::string res = "ActorPosition: m_RowsCols{";
  for (unsigned int i = 0; i < m_RowsCols.size(); i++)
    res += vector2AsString (m_RowsCols[i]);
  res += "} ";
  res += "m_rotation: " + std::to_string (m_rotation);
  return res;
}

}
