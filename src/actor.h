//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef ACTOR_H
#define ACTOR_H

#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
#include "board.h"

typedef unsigned int ActorId;

class Actor
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  Actor();
  virtual ~Actor();
  const ActorId getId() const { return m_id; }
  sf::Sprite &getSprite() { return m_sprite; }
  static std::string getPositionAsString (sf::Vector2i);
  static std::string getBoundsAsString (sf::FloatRect);
  bool isSelected () const { return m_isSelected; }
  virtual void select() { m_isSelected = true; }
  virtual void deselect() { m_isSelected = false; }

protected:
  // Attributes ----------------------------------------------------------------
  ActorId m_id;
  sf::Sprite m_sprite;
  std::vector<flc_GameBoard::Cell*> m_occupiedCells; // links to the occupied cells on the battlefield
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  bool m_isSelected;
  // Methods -------------------------------------------------------------------
};

#include<actor.inl>

#endif // ACTOR_H
