//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef NEWGAMESTATE_H
#define NEWGAMESTATE_H

#include "state.h"

namespace flc_game
{

class NewGameState: public State
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  NewGameState (StateManager *stateManager, ApplicationContext context);
  virtual ~NewGameState();

  void init() {};
  void destroy() {};
  void processEvents (const sf::Event &e) {};
  void update (sf::Time &dt);
  void render() {};

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
};

}

#endif // NEWGAMESTATE_H
