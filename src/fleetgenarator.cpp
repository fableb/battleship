//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "fleetgenarator.h"

namespace flc_FeetGenerator
{

void createPositions (std::vector<ActorPosition> &boatPositions, unsigned int rows, unsigned int cols,  FleetGenarator::BoatWidth boatWidth);

FleetGenarator::FleetGenarator (unsigned int rows, unsigned int cols):
  m_rows{rows},
  m_cols{cols}
{
  setupPositions();
  //createFleet();
}

FleetGenarator::~FleetGenarator()
{
  //dtor
}

/*
  Creates all possible positions for each kind of boat on the battlefield
*/
void FleetGenarator::setupPositions()
{
  createPositions (m_destroyerPositions, m_rows, m_cols, FleetGenarator::Destroyer);
  std::cout << "Destroyer positions:  " << m_destroyerPositions.size() << std::endl;
  for (unsigned int i = 0; i < m_destroyerPositions.size(); i++)
  {
    std::cout << "\t" << std::setw (3) << i << ":" << m_destroyerPositions[i].getActorPositionAsString() << std::endl;
  }

  createPositions (m_submarinePositions, m_rows, m_cols, FleetGenarator::Submarine);
  std::cout << "Submarine positions:  " << m_submarinePositions.size() << std::endl;
  for (unsigned int i = 0; i < m_submarinePositions.size(); i++)
  {
    std::cout << "\t" << std::setw (3) << i << ":" << m_submarinePositions[i].getActorPositionAsString() << std::endl;
  }

  createPositions (m_cruiserPositions, m_rows, m_cols, FleetGenarator::Cruiser);
  std::cout << "Cruiser positions:    " << m_cruiserPositions.size() << std::endl;
  for (unsigned int i = 0; i < m_cruiserPositions.size(); i++)
  {
    std::cout << "\t" << std::setw (3) << i << ":" << m_cruiserPositions[i].getActorPositionAsString() << std::endl;
  }

  createPositions (m_battleshipPositions, m_rows, m_cols, FleetGenarator::Battleship);
  std::cout << "Battleship positions: " << m_battleshipPositions.size() << std::endl;
  for (unsigned int i = 0; i < m_battleshipPositions.size(); i++)
  {
    std::cout << "\t" << std::setw (3) << i << ":" << m_battleshipPositions[i].getActorPositionAsString() << std::endl;
  }

  createPositions (m_carrierPositions, m_rows, m_cols, FleetGenarator::Carrier);
  std::cout << "Carrier positions:    " << m_carrierPositions.size() << std::endl;
  for (unsigned int i = 0; i < m_carrierPositions.size(); i++)
  {
    std::cout << "\t" << std::setw (3) << i << ":" << m_carrierPositions[i].getActorPositionAsString() << std::endl;
  }
}

/*
 Creates a fleet and setup each boat position
*/
void FleetGenarator::createFleet()
{
  std::srand (std::time (nullptr)); // use current time as seed for random generator
  std::vector<ActorPosition> posInList; // boats positions already in the fleet

  m_fleet.clear();

  // Carrier
  int randomVal = std::rand() % m_carrierPositions.size();
  ActorPosition pos = m_carrierPositions[randomVal];
  std::cout << "Carrier position  : " << pos.getActorPositionAsString() << std::endl;
  m_fleet.insert (std::pair<Boat::BoatType, ActorPosition> (Boat::BoatType::Carrier, pos));
  posInList.push_back (pos);

  // Battleship
  int numCollisions = 0;
  do
  {
    numCollisions = 0;
    randomVal = std::rand() % m_battleshipPositions.size();
    std::cout << "randomVal:" << randomVal << std::endl;
    pos = m_battleshipPositions[randomVal];
    for (unsigned int i = 0; i < posInList.size(); i++)
    {
      if (pos.intersects (posInList[i]))
        numCollisions++;
    }
  }
  while (numCollisions > 0);
  std::cout << "Battleship collisions: " << numCollisions << std::endl;
  std::cout << "Battleship position  : " << pos.getActorPositionAsString() << std::endl;
  m_fleet.insert (std::pair<Boat::BoatType, ActorPosition> (Boat::BoatType::Battleship, pos));
  posInList.push_back (pos);

  // Cruiser
  numCollisions = 0;
  do
  {
    numCollisions = 0;
    randomVal = std::rand() % m_cruiserPositions.size();
    std::cout << "randomVal:" << randomVal << std::endl;
    pos = m_cruiserPositions[randomVal];
    for (unsigned int i = 0; i < posInList.size(); i++)
    {
      if (pos.intersects (posInList[i]))
        numCollisions++;
    }
  }
  while (numCollisions > 0);
  std::cout << "Cruiser collisions: " << numCollisions << std::endl;
  std::cout << "Cruiser position  : " << pos.getActorPositionAsString() << std::endl;
  m_fleet.insert (std::pair<Boat::BoatType, ActorPosition> (Boat::BoatType::Cruiser, pos));
  posInList.push_back (pos);

  // Submarine
  numCollisions = 0;
  do
  {
    numCollisions = 0;
    randomVal = std::rand() % m_submarinePositions.size();
    std::cout << "randomVal:" << randomVal << std::endl;
    pos = m_submarinePositions[randomVal];
    for (unsigned int i = 0; i < posInList.size(); i++)
    {
      if (pos.intersects (posInList[i]))
        numCollisions++;
    }
  }
  while (numCollisions > 0);
  std::cout << "Submarine collisions: " << numCollisions << std::endl;
  std::cout << "Submarine position  : " << pos.getActorPositionAsString() << std::endl;
  m_fleet.insert (std::pair<Boat::BoatType, ActorPosition> (Boat::BoatType::Submarine, pos));
  posInList.push_back (pos);

  // Destroyer
  numCollisions = 0;
  do
  {
    numCollisions = 0;
    randomVal = std::rand() % m_destroyerPositions.size();
    std::cout << "randomVal:" << randomVal << std::endl;
    pos = m_destroyerPositions[randomVal];
    for (unsigned int i = 0; i < posInList.size(); i++)
    {
      if (pos.intersects (posInList[i]))
        numCollisions++;
    }
  }
  while (numCollisions > 0);
  std::cout << "Destroyer collisions: " << numCollisions << std::endl;
  std::cout << "Destroyer position  : " << pos.getActorPositionAsString() << std::endl;
  m_fleet.insert (std::pair<Boat::BoatType, ActorPosition> (Boat::BoatType::Destroyer, pos));
  posInList.push_back (pos);
}

// Creates positions for a type of boat
void createPositions (std::vector<ActorPosition> &boatPositions, unsigned int rows, unsigned int cols, FleetGenarator::BoatWidth boatWidth)
{
  // Horizontal positions
  for (unsigned int j = 0; j < rows; j++)
  {
    for (unsigned int i = 0; i < cols - boatWidth + 1; i++)
    {
      ActorPosition pos = ActorPosition (j, i, boatWidth);
      boatPositions.push_back (pos);
    }
  }

  // Vertical positions
  for (unsigned int i = 0; i < cols; i++)
  {
    for (unsigned int j =  0; j < rows - boatWidth + 1; j++)
    {
      ActorPosition pos = ActorPosition (j, i, boatWidth, true);
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // !!! We reverse the entries because a boat rotation is bottom-up !!!
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      std::reverse (pos.getRowsCols().begin(), pos.getRowsCols().end());
      boatPositions.push_back (pos);
    }
  }
}

}
