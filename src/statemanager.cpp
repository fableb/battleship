//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "statemanager.h"

namespace flc_game
{

StateManager::StateManager (ApplicationContext context):
  m_context{context},
  m_state{nullptr},
  m_introState{nullptr},
  m_newGameState{nullptr},
  m_gameState{nullptr}
{
  setState (State::StateType::Intro);
}

StateManager::~StateManager()
{
  if (m_introState != nullptr)
    delete m_introState;

  if (m_newGameState != nullptr)
    delete m_newGameState;

  if (m_gameState != nullptr)
    delete m_gameState;
}

void StateManager::setState (State::StateType state)
{
  switch (state)
  {
  case State::StateType::Intro:
    if (m_introState != nullptr)
    {
      delete m_introState;
      m_introState = nullptr;
    }
    m_introState = new IntroState (this, m_context);
    m_state = m_introState;
    break;
  case State::StateType::NewGame:
    if (m_newGameState != nullptr)
    {
      delete m_newGameState;
      m_newGameState = nullptr;
    }
    m_newGameState = new NewGameState (this, m_context);
    m_state = m_newGameState;
    break;
  case State::StateType::GameState:
    if (m_gameState != nullptr)
    {
      delete m_gameState;
      m_gameState = nullptr;
    }
    m_gameState = new GameState (this, m_context);
    m_state = m_gameState;
    break;
  }
}
void StateManager::run()
{
  sf::Clock clock;
  sf::Time dt; // for frame rate usage
  sf::Event e; // one envent queue for all states

  while (m_context.m_window->isOpen())
  {
    dt = clock.restart();
    m_state->update (dt);
    m_state->render();
    while (m_context.m_window->pollEvent (e))
    {
      m_state->processEvents (e);
    }
  }
}

}

