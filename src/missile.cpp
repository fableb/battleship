//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "missile.h"

Missile::Missile (unsigned int id):
  m_status{Missile::Status::None},
  m_targetRowCol(),
  m_explosion()
{
  m_id = id;
}

Missile::~Missile()
{
}

void Missile::setStatus (Missile::Status status)
{
  m_status = status;
//  if (status == Missile::Status::Hit)
//    m_sprite.setTextureRect (sf::IntRect (TILE_SIZE, 0, TILE_SIZE, TILE_SIZE));
}

void Missile::updateOccupiedCells (flc_GameBoard::Battlefield &b)
{
  // Reset
  m_occupiedCells.clear();

  // Update occupied cells on battlefield
  for (unsigned int i = 0; i < b.getNumRows(); i++)
  {
    for (unsigned int j = 0; j < b.getNumCols(); j++)
    {
      sf::FloatRect cellBounds = static_cast<sf::FloatRect> (b.getCell (i, j).getBounds());
      if (m_sprite.getGlobalBounds().intersects (cellBounds))
      {
        m_occupiedCells.push_back (&b.getCell (i, j));
      }
    }
  }
  std::cout << occupiedCellsAsString() << std::endl;
}

std::string Missile::occupiedCellsAsString()
{
  std::string res = "";
  for (unsigned int i = 0 ; i < m_occupiedCells.size(); i++)
  {
    res += "RowCol[" + std::to_string (m_occupiedCells[i]->getRowColl().x) + "," + std::to_string (m_occupiedCells[i]->getRowColl().y) + "] ";
  }
  return res;
}
