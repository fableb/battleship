//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef BOARD_H
#define BOARD_H

#include <SFML/Graphics.hpp>
#include "battlefield.h"

namespace flc_GameBoard
{

class Board
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  Board();
  virtual ~Board();
  sf::Sprite &getSprite() { return m_sprite; }
  Battlefield &getBattlefield() { return m_battlefield; }
  unsigned int getMarginSize() { return m_marginSize; }
  void setMarginSize (unsigned int margin) { m_marginSize = margin; }

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
private:
  // Attributes ----------------------------------------------------------------
  sf::Sprite m_sprite;
  Battlefield m_battlefield;
  unsigned int m_marginSize;
  // Methods -------------------------------------------------------------------
};

}

#endif // BOARD_H
