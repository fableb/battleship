//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef BATTLEFIELD_H
#define BATTLEFIELD_H

#include <SFML/Graphics.hpp>
#include "cell.h"

namespace flc_GameBoard
{

class Battlefield
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  Battlefield();
  virtual ~Battlefield();
  void setBounds (sf::IntRect bounds, unsigned int numRows, unsigned int numCols, unsigned int tileSize);
  sf::IntRect getBounds () const { return m_bounds; }
  unsigned int getNumRows() const { return m_numRows; }
  unsigned int getNumCols() const { return m_numCols; }
  unsigned int getTileSize() const { return m_tileSize; }
  Cell &getCell (unsigned int row, unsigned int col) const { return m_cells[row][col]; }
  Cell &getCell (sf::Vector2i coord) const { return m_cells[coord.x][coord.y]; }

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
private:
  // Attributes ----------------------------------------------------------------
  sf::IntRect m_bounds;
  unsigned int m_numRows;
  unsigned int m_numCols;
  unsigned int m_tileSize;
  Cell **m_cells;
  // Methods -------------------------------------------------------------------
  void initBounds ();
  void destroyBounds();
};

}

#endif // BATTLEFIELD_H
