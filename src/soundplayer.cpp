//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "soundplayer.h"

SoundPlayer::SoundPlayer():
  m_soundBuffers(),
  m_sounds()
{
  m_soundBuffers.load (SoundEffects::Explosion5, "assets/sounds/explosion5.wav");
  m_soundBuffers.load (SoundEffects::Explosion7, "assets/sounds/explosion7.wav");
}

SoundPlayer::~SoundPlayer()
{
  //dtor
}

void SoundPlayer::play (SoundEffects::ID effect)
{
  m_sounds.push_back (sf::Sound (m_soundBuffers.get (effect)));
  m_sounds.back().play();
}

void SoundPlayer::play (SoundEffects::ID effect, sf::Vector2f position)
{

}

void SoundPlayer::removeStoppedSounds()
{
  m_sounds.remove_if ([] (const sf::Sound & s)
  {
    return s.getStatus() == sf::Sound::Stopped;
  });
}

void SoundPlayer::setListenerPosition (sf::Vector2f position)
{

}

sf::Vector2f SoundPlayer::getListenerPosition() const
{
  return sf::Vector2f (0, 0);
}
