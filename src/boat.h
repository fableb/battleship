//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef BOAT_H
#define BOAT_H

#include <iostream>
#include "actor.h"
#include "battlefield.h"

class Boat : public Actor
{
public:
  // Attributes ----------------------------------------------------------------
  enum BoatType
  {
    Destroyer,
    Submarine,
    Cruiser,
    Battleship,
    Carrier
  };
  enum Health
  {
    DestroyerHealth = 2,
    SubmarineHealth = 3,
    CruiserHealth = 3,
    BattleshipHealth = 4,
    CarrierHealth = 5
  };
  // Methods -------------------------------------------------------------------
  Boat();
  Boat (const Boat &boat); // copy constructor
  virtual ~Boat();
  BoatType getBoatType() { return m_type; }
  void setBoatType (const BoatType v);
  int getHealth() { return m_health; }
  void decrementHealth() { m_health--; };
  void rotate (unsigned int tileSize);
  bool getRotation() { return m_rotation; }
  std::string getName();
  void updateOccupiedCells (flc_GameBoard::Battlefield &b);
  std::vector<flc_GameBoard::Cell*> getOccupiedCells() const { return m_occupiedCells; }
  std::string occupiedCellsAsString();
  bool isHitted (sf::Vector2i targetRowCol);

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  static const unsigned int m_numBoatTypes = 5;
  const std::string m_names[m_numBoatTypes] =
  {
    "Destroyer",
    "Submarine",
    "Cruiser",
    "Battleship",
    "Carrier"
  };
  BoatType m_type;
  int m_health;
  bool m_rotation; // rotation flag
  // Methods -------------------------------------------------------------------
};

#endif // BOAT_H
