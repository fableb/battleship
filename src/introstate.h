//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef INTROSTATE_H
#define INTROSTATE_H

#include <iostream>
#include "globals.h"
#include "state.h"
#include "musicplayer.h"
#include "container.h"
#include "button.h"

namespace flc_game
{

class IntroState : public State
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  IntroState (StateManager *stateManager, ApplicationContext context);
  virtual ~IntroState();

  void init() {};
  void destroy() {};
  void processEvents (const sf::Event &e);
  void update (sf::Time &dt);
  void render();

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  sf::Sprite m_sprite;
  bool m_isPaused;
  sf::Vector2i m_currentMousePosition; // current mouse position in the game
  std::wstring m_playButtonTextStr = L"Jouer";
  flc_GUI::Container m_GUIContainer;

  // Methods -------------------------------------------------------------------
  void handlePlayerInput (sf::Keyboard::Key key, bool isPressed);
  void handlePlayerMouse (sf::Event::MouseButtonEvent mouseButtonEvent, sf::Vector2i &mousePosition, bool isButtonPressed);
};

}

#endif // INTROSTATE_H
