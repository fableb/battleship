//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include <SFML/Graphics.hpp>
#include "state.h"
#include "introstate.h"
#include "newgamestate.h"
#include "gamestate.h"

namespace flc_game
{

class StateManager
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  explicit StateManager (ApplicationContext context);
  virtual ~StateManager();
  void setState (State::StateType state);
  void run();

  void init() { m_state->init(); };
  void destroy() { m_state->destroy(); };
  void processEvents (const sf::Event &e) { m_state->processEvents (e); };
  void update (sf::Time &dt) { m_state->update (dt); };
  void render() { m_state->render(); };

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  ApplicationContext m_context;
  State *m_state; // current state

  IntroState *m_introState; // game title state
  NewGameState *m_newGameState; // start or restart game state
  GameState *m_gameState; // game logic and display state

  // Methods -------------------------------------------------------------------
};

}

#endif // STATEMANAGER_H
