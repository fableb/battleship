//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef MUSICPLAYER_H
#define MUSICPLAYER_H

#include <map>
#include <SFML/Audio.hpp>

namespace Music
{
enum ID
{
  MenuTheme,
  GameTheme
};
}

class MusicPlayer: private sf::NonCopyable
{
public:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------
  MusicPlayer();
  virtual ~MusicPlayer();
  void play (Music::ID theme);
  void stop();
  void setPaused (bool paused);
  void setVolume (float volume); // volume in [0,100]

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  sf::Music m_music;
  std::map<Music::ID, std::string> m_filenames;
  float m_volume;
  // Methods -------------------------------------------------------------------
};

#endif // MUSICPLAYER_H
