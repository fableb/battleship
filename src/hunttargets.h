//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef HUNTTARGETS_H
#define HUNTTARGETS_H

#include <vector>
#include <string>
#include <algorithm>

class HuntTargets
{
public:
  // Attributes ----------------------------------------------------------------
  enum class Directions
  {
    Up,
    Left,
    Right,
    Down
  };
  // Methods -------------------------------------------------------------------
  HuntTargets ();
  virtual ~HuntTargets();
  unsigned int size() const;
  bool isEmpty() const;
  void setRoot (unsigned int root) { m_root = root; }
  unsigned int getRoot() const { return m_root; }
  void addChild (unsigned int child, Directions direction);
  void removeChild (unsigned int id);
  void clear();
  void clearChildren (unsigned int id);
  std::vector<unsigned int> getTargets();
  std::string toString();

protected:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  unsigned int m_root;
  std::vector<unsigned int> m_up;
  std::vector<unsigned int> m_left;
  std::vector<unsigned int> m_right;
  std::vector<unsigned int> m_down;
  // Methods -------------------------------------------------------------------
};

#endif // HUNTTARGETS_H
