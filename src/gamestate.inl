//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
template<class T>
int getSelectedActor (std::vector<std::unique_ptr<T>> &actors, sf::Vector2i &mousePosition)
{
  int res = -1;
  for (unsigned int i = 0; i < actors.size(); i++)
  {
    if (actors[i]->getSprite().getGlobalBounds().contains (mousePosition.x, mousePosition.y))
    {
      res = i;
      break;
    }
  }
  return res;
}

template <typename T>
bool contains (std::vector<T> &container, const T &item)
{
  return std::find (std::begin (container), std::end (container), item) != std::end (container);
}

template <typename T>
bool contains (std::deque<T> &container, const T &item)
{
  return std::find (std::begin (container), std::end (container), item) != std::end (container);
}
