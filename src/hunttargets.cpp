//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "hunttargets.h"

HuntTargets::HuntTargets():
  m_root{0}
{
  //ctor
}

HuntTargets::~HuntTargets()
{
  //dtor
}

unsigned int HuntTargets::size() const
{
  return m_up.size() + m_left.size() + m_right.size() + m_down.size();
}

bool HuntTargets::isEmpty() const
{
  if (size() == 0)
    return true;
  else
    return false;
}

void HuntTargets::clear()
{
  m_up.clear();
  m_left.clear();
  m_right.clear();
  m_down.clear();
}

void HuntTargets::addChild (unsigned int child, Directions direction)
{
  switch (direction)
  {
  case Directions::Up:
    m_up.push_back (child);
    break;
  case Directions::Left:
    m_left.push_back (child);
    break;
  case Directions::Right:
    m_right.push_back (child);
    break;
  case Directions::Down:
    m_down.push_back (child);
    break;
  }
}

void HuntTargets::removeChild (unsigned int id)
{
  for (unsigned int i = 0; i < m_up.size(); i++)
    if (m_up[i] == id)
    {
      m_up.erase (m_up.begin() + i);
      return;
    }

  for (unsigned int i = 0; i < m_left.size(); i++)
    if (m_left[i] == id)
    {
      m_left.erase (m_left.begin() + i);
      return;
    }

  for (unsigned int i = 0; i < m_right.size(); i++)
    if (m_right[i] == id)
    {
      m_right.erase (m_right.begin() + i);
      return;
    }

  for (unsigned int i = 0; i < m_down.size(); i++)
    if (m_down[i] == id)
    {
      m_down.erase (m_down.begin() + i);
      return;
    }
}

void HuntTargets::clearChildren (unsigned int parentId)
{
  for (unsigned int i = 0; i < m_up.size(); i++)
    if (m_up[i] == parentId)
    {
      m_up.clear();
      return;
    }

  for (unsigned int i =  0; i < m_left.size(); i++)
    if (m_left[i] == parentId)
    {
      m_left.clear();
      return;
    }

  for (unsigned int i = 0; i < m_right.size(); i++)
    if (m_right[i] == parentId)
    {
      m_right.clear();
      return;
    }

  for (unsigned int i = 0; i < m_down.size(); i++)
    if (m_down[i] == parentId)
    {
      m_down.clear();
      return;
    }
}

std::vector<unsigned int> HuntTargets::getTargets()
{
  std::vector<unsigned int> r;
  r.insert (r.end(), m_up.begin(), m_up.end());
  r.insert (r.end(), m_left.begin(), m_left.end());
  r.insert (r.end(), m_right.begin(), m_right.end());
  r.insert (r.end(), m_down.begin(), m_down.end());
  return r;
}

std::string HuntTargets::toString()
{
  std::string r = "root:" + std::to_string (m_root);
  r += " up:";
  for (unsigned int e : m_up)
    r += std::to_string (e) + " ";

  r += " left:";
  for (unsigned int e : m_left)
    r += std::to_string (e) + " ";

  r += " right:";
  for (unsigned int e : m_right)
    r += std::to_string (e) + " ";

  r += " down:";
  for (unsigned int e : m_down)
    r += std::to_string (e) + " ";

  return r;
}

