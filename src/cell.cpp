//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "cell.h"

namespace flc_GameBoard
{

Cell::Cell ():
  m_row{0},
  m_col{0},
  m_contentType{ContentType::None}
{

}

Cell::Cell (sf::IntRect bounds):
  Cell()
{
  m_bounds = bounds;
}

Cell::~Cell()
{
  //dtor
}

sf::Vector2i Cell::getRowColl() const
{
  return sf::Vector2i (m_row, m_col);
}

sf::Vector2f Cell::getPosition() const
{
  return sf::Vector2f (m_bounds.left, m_bounds.top);
}

}
