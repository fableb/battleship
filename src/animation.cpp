//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "animation.h"

Animation::Animation():
  m_sprite(),
  m_frameSize(),
  m_numFrames{0},
  m_currentFrame{0},
  m_duration{sf::Time::Zero},
  m_elapsedTime{sf::Time::Zero},
  m_repeat{false}
{
  //ctor
}

Animation::Animation (const sf::Texture& texture):
  m_sprite (texture),
  m_frameSize(),
  m_numFrames{0},
  m_currentFrame{0},
  m_duration{sf::Time::Zero},
  m_elapsedTime{sf::Time::Zero},
  m_repeat{false}
{

}

Animation::~Animation()
{
  //dtor
}

bool Animation::isFinished() const
{
  return m_currentFrame >= m_numFrames;
}

sf::FloatRect Animation::getLocalBounds() const
{
  return sf::FloatRect (getOrigin(), static_cast<sf::Vector2f> (getFrameSize()));
}

sf::FloatRect Animation::getGlobalBounds() const
{
  return getTransform().transformRect (getLocalBounds());
}

void Animation::draw (sf::RenderTarget& target, sf::RenderStates states) const
{
  states.transform *= getTransform();
  target.draw (m_sprite, states);
}

void Animation::update (sf::Time dt)
{
  sf::Time timePerFrame = m_duration / static_cast<float> (m_numFrames);
  m_elapsedTime += dt;

  sf::Vector2i textureBounds (m_sprite.getTexture()->getSize());
  sf::IntRect textureRect = m_sprite.getTextureRect();

  if (m_currentFrame == 0) // beginning of sprite
  {
    textureRect = sf::IntRect (0, 0, m_frameSize.x, m_frameSize.y);
  }

  // loop over all tiles in texture
  while (m_elapsedTime >= timePerFrame && (m_currentFrame <= m_numFrames || m_repeat))
  {
    textureRect.left += textureRect.width;
    if (textureRect.left + textureRect.width > textureBounds.x)
    {
      textureRect.left = 0;
      textureRect.top += textureRect.height;
    }

    m_elapsedTime -= timePerFrame;

    if (m_repeat)
    {
      m_currentFrame = (m_currentFrame + 1) % m_numFrames;
      if (m_currentFrame == 0)
        textureRect = sf::IntRect (0, 0, m_frameSize.x, m_frameSize.y);
    }
    else
    {
      m_currentFrame++;
    }
  }
  m_sprite.setTextureRect (textureRect);
}
