//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#include "button.h"

namespace flc_GUI
{

Button::Button (const FontHolder &fonts, const TextureHolder &textures):
  m_callback(),
  m_normalTexture (textures.get (Textures::ButtonOrange1)),
  m_selectedTexture (textures.get (Textures::ButtonOrange0)),
  m_pressedTexture (textures.get (Textures::ButtonOrange2)),
  m_sprite(),
  m_text ("", fonts.get (Fonts::Normal), 19),
  m_isToggle{false}
{
  m_sprite.setTexture (m_normalTexture);
  m_text.setFillColor (sf::Color::Black);
}

Button::~Button()
{
  //dtor
}

void Button::setCallback (std::function<void()>callback)
{
  m_callback = std::move (callback);
}

void Button::setText (const std::string& text)
{
  m_text.setString (text);
  centerOrigin (m_text);
}

void Button::setText (const std::wstring& text)
{
  m_text.setString (text);
  centerOrigin (m_text);
}

void Button::setTextColor (sf::Color color)
{
  m_text.setFillColor (color);
}

void Button::setToggle (bool flag)
{
  m_isToggle = flag;
}

bool Button::isSelectable() const
{
  return true;
}

void Button::select()
{
  Component::select();
  m_sprite.setTexture (m_selectedTexture);
}

void Button::deselect()
{
  Component::deselect();
  m_sprite.setTexture (m_normalTexture);
}

void Button::activate()
{
  Component::activate();

  // If we are toggle then we should show that the button is pressed and thus "toggled".
  if (m_isToggle)
    m_sprite.setTexture (m_pressedTexture);

  if (m_callback && isVisible())
    m_callback();

  // If we are not a toggle then deactivate the button since we are just momentarily activated.
  if (!m_isToggle)
    deactivate();
}

void Button::deactivate()
{
  Component::deactivate();

  if (m_isToggle)
  {
    // Reset texture to right one depending on if we are selected or not.
    if (isSelected())
      m_sprite.setTexture (m_selectedTexture);
    else
      m_sprite.setTexture (m_normalTexture);
  }
}

void Button::handleEvent (const sf::Event& event)
{

}

sf::FloatRect Button::getGlobalBounds() const
{
  return m_sprite.getGlobalBounds();

} sf::FloatRect Button::getLocalBounds() const
{
  return m_sprite.getLocalBounds();
}

void Button::draw (sf::RenderTarget& target, sf::RenderStates states) const
{
  if (m_isVisible)
  {
    states.transform *= getTransform();
    target.draw (m_sprite, states);
    target.draw (m_text, states);
  }
}

void Button::setPosition (const sf::Vector2f& position)
{
  setPosition (position.x, position.y);
}

// Overload Transformable::setPosition() because it affects sprite and text
// position in getGlobalBounds().
void Button::setPosition (float x, float y)
{
  m_sprite.setPosition (x, y);
  sf::FloatRect bounds = m_sprite.getLocalBounds();
  m_text.setPosition (x + bounds.width / 2.f, y + bounds.height / 2.f);
}

}
