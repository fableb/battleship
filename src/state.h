//
// Copyright (C) 2020 Fabrice LEBEL
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
// Contact: fabrice.lebel.pro@outlook.com
//
#ifndef STATE_H
#define STATE_H

#include <SFML/Graphics.hpp>
#include "applicationcontext.h"

namespace flc_game
{

class StateManager; // to avoid circular inclusion with 'statemanager.h'

class State
{
public:
  // Attributes ----------------------------------------------------------------
  enum class StateType
  {
    Intro,
    NewGame,
    GameState
  };
  // Methods -------------------------------------------------------------------
  State (StateManager *stateManager, ApplicationContext context);
  virtual ~State();
  void setState (StateType state);

  // Init and cleanup methods
  virtual void init() = 0;
  virtual void destroy() = 0;
  // Transitions, triggers, requests
  virtual void processEvents (const sf::Event &e) = 0;
  virtual void update (sf::Time &dt) = 0;
  virtual void render() = 0;

protected:
  // Attributes ----------------------------------------------------------------
  StateManager *m_stateManager;
  ApplicationContext m_context;
  // Methods -------------------------------------------------------------------

private:
  // Attributes ----------------------------------------------------------------
  // Methods -------------------------------------------------------------------

};

}

#endif // STATE_H
